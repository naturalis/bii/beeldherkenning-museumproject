FROM tensorflow/tensorflow:2.7.0-gpu-jupyter
#FROM tensorflow/tensorflow:2.0.1-gpu-py3-jupyter

COPY ./requirements/requirements.txt /app/requirements.txt
WORKDIR /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
#CMD ["bash"]





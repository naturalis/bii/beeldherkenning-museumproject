import os, argparse, glob, shutil
from PIL import Image
# from lib import baseclass, utils, dataset

class ImageConvertChannels:

    valid_bands = [('R','G','B')]

    def set_root_dir(self,root_dir):
        self.root_dir = root_dir

    def set_current_image_file(self,current_image_file):
        self.current_image_file = current_image_file

    def get_bands(self,file):
        im = Image.open(file)
        return Image.Image.getbands(im)

    def convert_image(self):
        shutil.copyfile(self.current_image_file, self.current_image_file + ".preconvert")
        im = Image.open(self.current_image_file)
        out = im.convert("RGB")
        out.save(self.current_image_file)

        pre = Image.Image.getbands(im)
        im = Image.open(self.current_image_file)
        print("{}: {} --> {}".format(self.current_image_file,pre,Image.Image.getbands(im)))

    def go_convert(self):
        self.progress = 0
        # for extension in [".jpg",".jpeg",".png"]:
        for extension in [".jpg",".jpeg"]:
            path = self.root_dir + '/**/*' + extension
            for filename in glob.iglob(path, recursive=True):
                if os.path.isfile(filename):
                    try:
                        self.set_current_image_file(filename)
                        if not self.get_bands(self.current_image_file) in self.valid_bands:
                            self.convert_image()
                    except Exception as e:
                        print("filename: {}; error: {}".format(filename,str(e)))

        print("done")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--image_root",type=str, required=True)
    args = parser.parse_args()

    img = ImageConvertChannels()
    img.set_root_dir(args.image_root)
    img.go_convert()

import argparse, csv, json, re, os
import urllib.parse, urllib.request
from lib import logclass

'''
This script aims to resolve scientific names to accepted scientific names from GBIF.

The script takes to parameters:

--input_file (mandatory)
--output_file (optional; defaults to ./output.tsv)

The input should be a text file with a list of scientifc names, one per line (lines that start with # are ignored).

The script:

1. Pre-processes names: '(f)' and '(m)' are removed from the string, as well as other possible artifacts.
2. Looks up the cleaned up names through the GBIF's name match API, resulting in a list of possible matches.
   Responses typically have one main match, and optionally an array of alternative matches. Each match comes
   with a confidence score.
3. From all matches, the one with the highest confidence is selected, providing that
   - Its rank is in self.accepted_ranks (skipped ranks are printed to the screen).
   - Its confidence score is higher than self.minimum_confidence_score.
4. If the match is a synonym, the accepted name is retrieved uing the species lookup API, using the speciesKey that comes with the match.
5. The results are then written to the output file. Output has six colums:
   - raw_input: input, as is
   - sanitized: input, cleaned
   - resolved_name: best match scientific name
   - resolved_canonical: best match nomen
   - resolved_status: best match status
   - resolved_rank: best match rank
   - match_confidence: best match confidence
   - resolved_matches_input: true if input is identical to match (full name or nomen)
   - class for training: suggested class to use for training
'''

class NameResolver():

    input_file = None
    output_file = None
    full_report_file = None
    full_report_unique_lines_only = True
    names = []
    input_lines = []
    class_col = 0
    skipped_lines = []
    csv_delimiter = ","
    keep_resolved_only = False

    minimum_confidence_score = 75
    valid_ranks = [ "FAMILY", "GENUS", "SPECIES", "SUBSPECIES", "VARIETY", "FORM" ]
    default_ranks = [ "SPECIES", "SUBSPECIES", "VARIETY", "FORM" ]
    accepted_ranks = []

    url_gbif_name_match = 'https://api.gbif.org/v1/species/match?verbose=true&name={name}'
    url_gbif_species_lookup = 'https://api.gbif.org/v1/species/{key}'

    def __init__(self):
        pr = os.environ.get("PROJECT_ROOT")

        if pr is None:
            raise ValueError("need a project root (PROJECT_ROOT missing from .env)")

        logfile = os.path.join(pr, "log", "general.log")

        if not os.path.exists(logfile):
            path = os.path.join(pr, "log")
            if not os.path.exists(path):
                os.mkdir(path)
                os.chmod(path,0o777)

            with open(logfile, 'w') as fp:
                pass

            os.chmod(logfile,0o777)

        self.logger = logclass.LogClass(self.__class__.__name__,logfile)

        self.accepted_ranks = self.default_ranks

        self.logger.info("minimum_confidence_score (default): {}".format(self.minimum_confidence_score))
        self.logger.info("accepted_ranks (default): {}".format(";".join(self.accepted_ranks)))
        self.logger.info("full_report_unique_lines_only (default): {}".format(self.full_report_unique_lines_only))
        self.logger.info("accepted_ranks (default): {}".format(self.accepted_ranks))

    def set_input_file(self,input_file):
        self.input_file = input_file
        self.logger.info("input: {}".format(self.input_file))

    def set_output_file(self,output_file):
        self.output_file = output_file
        self.logger.info("output: {}".format(self.output_file))

    def set_full_report_file(self,full_report_file):
        self.full_report_file = full_report_file
        self.logger.info("full_report_file: {}".format(self.full_report_file))

    def set_minimum_confidence_score(self,minimum_confidence_score):
        self.minimum_confidence_score = minimum_confidence_score
        self.logger.info("minimum_confidence_score set to: {}".format(self.minimum_confidence_score))

    def reset_accepted_ranks(self):
        self.accepted_ranks = []

    def set_accepted_rank(self,accepted_rank):
        if accepted_rank in self.valid_ranks:
            self.accepted_ranks.append(accepted_rank)
            self.accepted_ranks = list(set(self.accepted_ranks))
            self.logger.info("accepted_ranks: {}".format(";".join(self.accepted_ranks)))
        else:
            raise ValueError("invalid rank: {}".format(accepted_rank))

    def set_full_report_unique_lines_only(self,full_report_unique_lines_only):
        self.full_report_unique_lines_only = full_report_unique_lines_only
        self.logger.info("report_unique_lines_only: {}".format(self.full_report_unique_lines_only))

    def set_csv_delimiter(self,csv_delimiter):
        self.csv_delimiter = "\t" if csv_delimiter =='[tab]' else csv_delimiter
        self.logger.info("csv_delimiter: {}".format(self.csv_delimiter))

    def set_keep_resolved_only(self,keep_resolved_only):
        self.keep_resolved_only = keep_resolved_only
        self.logger.info("keep_resolved_only: {}".format(self.keep_resolved_only))

    def read_input_file(self):
        with open(self.input_file, 'r', encoding='utf-8-sig') as csv_file:
            # dialect = csv.Sniffer().sniff(csv_file.read(1024), delimiters=";,\t")
            # csv_file.seek(0)
            # reader = csv.reader(csv_file, dialect)
            reader = csv.reader(csv_file, delimiter=self.csv_delimiter)
            self.input_lines = list(reader)

        self.names = [ { "raw" : x[self.class_col] } for x in self.input_lines]

        # print(self.input_lines)
        # print(self.names)

        self.logger.info("read {} names".format(len(self.names)))

    def preprocess_names(self):
        for name in self.names:
            self.current_name = name
            self.preprocess_name()

    def preprocess_name(self):
        p = [x for x in self.names if x["raw"]==self.current_name["raw"] and "sanitized" in x ]

        if len(p)>0:
            self.current_name["sanitized"] = p[0]["sanitized"]
        else:
            replacements = [
                {"element" : "(f)", "replacement" : "" },
                {"element" : "(m)", "replacement" : "" },
                {"element" : "♀", "replacement" : "" },
                {"element" : "♂", "replacement" : "" },
                {"element" : "_", "replacement" : " " }
            ]

            self.current_name["sanitized"] = self.current_name["raw"]

            for replacement in replacements:
                self.current_name["sanitized"] = self.current_name["sanitized"].replace(replacement["element"],replacement["replacement"])

            self.current_name["sanitized"] = re.sub(r"\s+", " ", self.current_name["sanitized"])
            self.current_name["sanitized"] = self.current_name["sanitized"].strip().capitalize()

    def gbif_lookup_names(self):
        i=0
        for name in self.names:
            self.current_name = name
            self.gbif_lookup_name()
            self.logger.info("{} {} --> {} [{}]".format(
                "v" if (
                    "best_match" in self.current_name and
                    self.current_name["best_match"]["status"]!="NONE" and
                    (
                        self.current_name["raw"]==self.current_name["best_match"]["resolved_name"] or
                        self.current_name["raw"]==self.current_name["best_match"]["resolved_nomen"] or
                        self.current_name["sanitized"]==self.current_name["best_match"]["resolved_name"] or
                        self.current_name["sanitized"]==self.current_name["best_match"]["resolved_nomen"]
                    )
                )
                else ".",
                self.current_name["sanitized"],
                self.current_name["best_match"]["resolved_name"]
                    if ("best_match" in self.current_name and self.current_name["best_match"]["status"]!="NONE") else "-",
                self.current_name["best_match"]["confidence"]
                    if ("best_match" in self.current_name and "confidence" in self.current_name["best_match"]) else "-"
                )
            )

            i+=1
            if i % 100 == 0:
                self.logger.debug("looked up {} name usages".format(i))

    # def get_similarity(self,note):
    #     return {x[0]: int(x[1]) if 1 in x else -1 for x in list(map(lambda x: x.lstrip().split('='),note.replace("Similarity:","").split(";")))}

    def get_values(self,data):
        return {
            "canonicalName": data["canonicalName"],
            "confidence": data["confidence"],
            "matchType": data["matchType"],
            "note": data["note"],
            # "similarity": self.get_similarity(data["note"]),
            "rank": data["rank"],
            "scientificName": data["scientificName"],
            "status": data["status"],
            "synonym": data["synonym"],
            "species": data["species"] if "species" in data else None,
            "speciesKey": data["speciesKey"] if "speciesKey" in data else None,
        }

    def gbif_lookup_name(self):
        p = [x for x in self.names if x["raw"]==self.current_name["raw"] and "best_match" in x ]

        if len(p)>0:
            self.current_name["best_match"] = p[0]["best_match"]
        else:
            best_match = { "confidence" : 0 }

            if "sanitized" in self.current_name:

                url = self.url_gbif_name_match.format(name=urllib.parse.quote(self.current_name["sanitized"]))

                response = urllib.request.urlopen(url)
                data = response.read()
                data = json.loads(data)

                # self.logger.info(self.current_name["sanitized"]);
                # self.logger.info(url);
                # self.logger.info(json.dumps(data, indent=4, sort_keys=True));

                best_confidence = 0
                best_match = {}

                if data["matchType"]=="NONE":
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.skipped_lines.append({"cause" : "no match","record" : self.current_name["raw"] })
                    self.logger.info("{}: no match".format(self.current_name["raw"]))
                    return

                if data["rank"] in self.accepted_ranks:
                    if data["confidence"] > best_confidence:
                        best_match = self.get_values(data)
                        best_confidence = data["confidence"]
                else:
                    self.skipped_lines.append({"cause" : "rank ({})".format(data["rank"]),"record" : self.current_name["raw"] })
                    self.logger.info("skipping {}".format(data["rank"]))

                if "alternatives" in data:
                    for alternative in data["alternatives"]:
                        if alternative["rank"] in self.accepted_ranks:
                            if alternative["confidence"] > best_confidence:
                                if alternative["rank"] in self.accepted_ranks:
                                    best_match = self.get_values(alternative)
                                    best_confidence = alternative["confidence"]
                                else:
                                    self.skipped_lines.append({
                                        "cause" : "rank of best alternative ({})".format(data["rank"]),
                                        "record" : self.current_name["raw"]
                                        })
                                    self.logger.info("skipping {}".format(data["rank"]))

                # self.logger.info(best_match)

                if not "scientificName" in best_match and not "canonicalName" in best_match:
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.skipped_lines.append({"cause" : "no match","record" : self.current_name["raw"] })
                    self.logger.info("{}: no match".format(self.current_name["raw"]))
                    return

                if best_confidence < self.minimum_confidence_score:
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.skipped_lines.append({"cause" : "low confidence ({})".format(best_confidence),"record" : self.current_name["raw"] })
                    self.logger.info("{}: best confidence too low ({})".format(self.current_name["raw"],best_confidence))
                    return

                best_match["resolved_name"] = best_match["scientificName"]
                best_match["resolved_nomen"] = best_match["canonicalName"]

                if best_match["status"]=="SYNONYM":
                    if "speciesKey" in best_match and not best_match["speciesKey"] == None:

                        url = self.url_gbif_species_lookup.format(key=urllib.parse.quote(str(best_match["speciesKey"])))
                        response = urllib.request.urlopen(url)
                        data = response.read()
                        data = json.loads(data)

                        # self.info(json.dumps(data, indent=4, sort_keys=True));

                        best_match["resolved_name"] = data["scientificName"]
                        best_match["resolved_nomen"] = data["canonicalName"]

                    elif "species" in best_match and not best_match["species"] == None:

                        best_match["resolved_name"] = best_match["species"]

                    else:

                        self.logger.warning("synonym without species!?: {}".format(url))

                elif best_match["status"]!="ACCEPTED":
                    self.logger.warning("weird status for {}: {}".format(self.current_name["raw"],best_match["status"]))

            else:
                self.logger.warning("{}: no sanitized name!?".format(self.current_name["raw"]))

            self.current_name["best_match"] = best_match

    def write_names(self):
        with open(self.output_file, 'w', newline='') as csvfile:
            delimiter=","
            writer = csv.writer(csvfile, delimiter=delimiter,quoting=csv.QUOTE_MINIMAL)

            for index, line in enumerate(self.input_lines):
                name = self.names[index]
                bm = name["best_match"] if "best_match" in name else {}

                if self.keep_resolved_only and not "resolved_name" in bm:
                    self.logger.info("skipping {} (no resolved name && keep_resolved_only)".format(name["raw"]))
                    continue

                best_match = bm["resolved_name"] if "resolved_name" in bm else name["raw"]
                row = line[1:]
                row.insert(0,best_match)
                row.append(line[0])
                writer.writerow(row)

        self.logger.info("wrote output: {}".format(self.output_file))

    def write_full_report_file(self):
        if self.full_report_file is None:
            self.logger.info("skipping full report")
            return

        header = [
            "raw_input",
            "sanitized",
            "resolved_name",
            "resolved_canonical",
            "resolved_status",
            "resolved_rank",
            "match_confidence",
            "resolved_matches_input",
            "class_for_training"
        ]

        with open(self.full_report_file, 'w', newline='') as csvfile:
            delimiter=","
            writer = csv.writer(csvfile, delimiter=delimiter,quoting=csv.QUOTE_MINIMAL)
            writer.writerow(header)

            done = []
            for name in self.names:

                if self.full_report_unique_lines_only:
                    if not name["sanitized"] in done:
                        done.append(name["sanitized"])
                    else:
                        continue

                bm = name["best_match"] if "best_match" in name else {}
                row = [
                    name["raw"],
                    name["sanitized"],
                    bm["resolved_name"] if "resolved_name" in bm else "",
                    bm["resolved_nomen"] if "resolved_nomen" in bm else "",
                    bm["status"] if "status" in bm else "",
                    bm["rank"] if "rank" in bm else "",
                    bm["confidence"] if "confidence" in bm else "",
                    (bm["resolved_name"]==name["raw"] or bm["resolved_nomen"]==name["raw"]) if "resolved_name" in bm else False,
                    bm["resolved_name"] if "resolved_name" in bm else name["raw"],
                ]

                writer.writerow(row)

        self.logger.info("wrote full report: {}".format(self.full_report_file))

    def print_skipped_lines(self):
        for item in self.skipped_lines:
            self.logger.info("skipped {record}: {cause}".format(record=item["record"],cause=item["cause"]))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file",type=str,required=True)
    parser.add_argument("--output_file",type=str,required=True)
    parser.add_argument("--full_report_file",type=str)
    parser.add_argument("--minimum_confidence_score",type=int,default=75)
    parser.add_argument("--report_all_lines", dest="full_report_unique_lines_only", action="store_false")
    parser.add_argument("--accepted_ranks",type=str,help="list of accepted ranks, separated by comma's")
    parser.add_argument("--csv_delimiter", type=str, default=",")
    parser.add_argument("--keep_resolved_only",action="store_true")
    args = parser.parse_args()

    n = NameResolver()
    n.set_input_file(args.input_file)
    n.set_output_file(args.output_file)
    n.set_csv_delimiter(args.csv_delimiter)
    n.set_keep_resolved_only(args.keep_resolved_only)

    if args.full_report_file:
        n.set_full_report_file(args.full_report_file)
        n.set_full_report_unique_lines_only(args.full_report_unique_lines_only)

    n.set_minimum_confidence_score(args.minimum_confidence_score)

    if args.accepted_ranks:
        n.reset_accepted_ranks()
        for item in map(lambda x: x.strip().upper(), args.accepted_ranks.split(",")):
            n.set_accepted_rank(item)

    n.read_input_file()
    n.preprocess_names()
    n.gbif_lookup_names()
    n.write_names()
    n.write_full_report_file()
    n.print_skipped_lines()
import argparse, csv, json, re, os
import urllib.parse, urllib.request
from lib import logclass

class GbifHigherTaxonLookup():

    input_file = None
    output_file = None
    input_lines = []
    skipped_lines = []
    class_col = 0
    minimum_confidence_score = 75
    url_gbif_name_match = 'https://api.gbif.org/v1/species/match?verbose=true&name={name}'
    url_gbif_key_lookup = 'https://api.gbif.org/v1/species/{key}'
    ranks = [ "class", "family", "genus", "kingdom", "order", "phylum", "species" ]
    rank_to_retrieve = None
    names = []
    scinames_by_key = []
    force_canonical = False

    def __init__(self):
        pr = os.environ.get("PROJECT_ROOT")

        if pr is None:
            raise ValueError("need a project root (PROJECT_ROOT missing from .env)")

        logfile = os.path.join(pr, "log", "general.log")

        if not os.path.exists(logfile):
            path = os.path.join(pr, "log")
            if not os.path.exists(path):
                os.mkdir(path)
                os.chmod(path,0o777)

            with open(logfile, 'w') as fp:
                pass

            os.chmod(logfile,0o777)

        self.logger = logclass.LogClass(self.__class__.__name__,logfile)
        self.logger.info("class_col (default): {}".format(self.class_col))

    def set_input_file(self,input_file):
        self.input_file = input_file
        self.logger.info("input_file: {}".format(self.input_file))

    def set_class_col(self,class_col):
        self.class_col = class_col
        self.logger.info("class_col: {}".format(self.class_col))

    def set_output_file(self,output_file):
        self.output_file = output_file
        self.logger.info("output_file: {}".format(self.output_file))

    def set_minimum_confidence_score(self,minimum_confidence_score):
        self.minimum_confidence_score = minimum_confidence_score
        self.logger.info("minimum_confidence_score set to: {}".format(self.minimum_confidence_score))

    def set_rank_to_retrieve(self,rank_to_retrieve):
        if not rank_to_retrieve in self.ranks:
            raise ValueError("invalid rank: {} (valid ranks: {})".format(rank_to_retrieve,";".join(self.ranks)))
        self.rank_to_retrieve = rank_to_retrieve
        self.logger.info("rank_to_retrieve: {}".format(self.rank_to_retrieve))

    def set_force_canonical(self,force_canonical):
        self.force_canonical = force_canonical

    def read_input_file(self):
        with open(self.input_file, 'r', encoding='utf-8-sig') as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(1024), delimiters=";,\t")
            csvfile.seek(0)
            reader = csv.reader(csvfile, dialect)
            self.input_lines = list(reader)

        self.names = [ { "raw" : x[self.class_col] } for x in self.input_lines]

        # print(self.input_lines)
        # print(self.names)

        self.logger.info("read {} names".format(len(self.names)))

    def preprocess_names(self):
        for name in self.names:
            self.current_name = name
            self.preprocess_name()

    def preprocess_name(self):
        p = [x for x in self.names if x["raw"]==self.current_name["raw"] and "sanitized" in x ]

        if len(p)>0:
            self.current_name["sanitized"] = p[0]["sanitized"]
        else:
            self.current_name["sanitized"] = self.current_name["raw"].capitalize()

            # replacements = [
            #     {"element" : "(f)", "replacement" : "" },
            #     {"element" : "(m)", "replacement" : "" },
            #     {"element" : "_", "replacement" : " " }
            # ]

            # for replacement in replacements:
            #     self.current_name["sanitized"] = self.current_name["sanitized"].replace(replacement["element"],replacement["replacement"])

            self.current_name["sanitized"] = re.sub(r"\s+", " ", self.current_name["sanitized"])

    def gbif_lookup_names(self):
        for name in self.names:
            self.current_name = name
            self.gbif_lookup_name()

            if "lookup" in self.current_name["best_match"]:
                if "scientificName" in self.current_name["best_match"]["lookup"] and not self.force_canonical:
                    t = self.current_name["best_match"]["lookup"]["scientificName"]
                elif "value" in self.current_name["best_match"]["lookup"]:
                    t = self.current_name["best_match"]["lookup"]["value"]
            else:
                t = "x"

            # t = self.current_name["best_match"]["lookup"]["value"] if "lookup" in self.current_name["best_match"] else "x"
            self.logger.info("{} --> {}".format(self.current_name["raw"],t))

    def get_values(self,data):
        r = {
            "canonicalName": data["canonicalName"],
            "confidence": data["confidence"],
            "matchType": data["matchType"],
            # "note": data["note"],
            # "similarity": self.get_similarity(data["note"]),
            "rank": data["rank"],
            "scientificName": data["scientificName"],
            "status": data["status"],
            # "synonym": data["synonym"],
            # "species": data["species"] if "species" in data else None,
            # "speciesKey": data["speciesKey"] if "speciesKey" in data else None,
        }


        if self.rank_to_retrieve in data:
            r["lookup"] = {
                    "rank": self.rank_to_retrieve,
                    "value": data[self.rank_to_retrieve]
                }

        if self.rank_to_retrieve + "Key" in data:
            r["lookup"]["key"] = data[self.rank_to_retrieve + "Key"]

        return r

    def gbif_lookup_name(self):
        p = [x for x in self.names if x["raw"]==self.current_name["raw"] and "best_match" in x ]

        if len(p)>0:
            self.current_name["best_match"] = p[0]["best_match"]
        else:
            best_match = { "confidence" : 0 }

            if "sanitized" in self.current_name:

                url = self.url_gbif_name_match.format(name=urllib.parse.quote(self.current_name["sanitized"]))

                response = urllib.request.urlopen(url)
                data = response.read()
                data = json.loads(data)

                # self.logger.info(json.dumps(data, indent=4, sort_keys=True))

                best_confidence = 0
                best_match = {}

                if data["matchType"]=="NONE":
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.skipped_lines.append({"cause" : "no match","record" : self.current_name["raw"] })
                    self.logger.info("{}: no match".format(self.current_name["raw"]))
                    return

                if data["confidence"] > best_confidence:
                    best_match = self.get_values(data)
                    best_confidence = data["confidence"]

                if "alternatives" in data:
                    for alternative in data["alternatives"]:
                        if alternative["confidence"] > best_confidence and alternative["rank"] in self.acceptable_ranks:
                            best_match = self.get_values(alternative)
                            best_confidence = alternative["confidence"]

                # self.logger.info(best_match)

                if not "lookup" in best_match:
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.skipped_lines.append({"cause" : "no match","record" : self.current_name["raw"] })
                    self.logger.info("{}: no match".format(self.current_name["raw"]))
                    return


                if best_confidence < self.minimum_confidence_score:
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.skipped_lines.append({"cause" : "low confidence ({})".format(best_confidence),"record" : self.current_name["raw"] })
                    self.logger.info("{}: best confidence too low ({})".format(self.current_name["raw"],best_confidence))
                    return

                # if best_match["status"]=="SYNONYM":
                #     if "speciesKey" in best_match:

                #         url = self.url_gbif_species_lookup.format(key=urllib.parse.quote(str(best_match["speciesKey"])))

                #         response = urllib.request.urlopen(url)
                #         data = response.read()
                #         data = json.loads(data)

                #         # self.info(json.dumps(data, indent=4, sort_keys=True));

                #         best_match["resolved_name"] = data["scientificName"]
                #         best_match["resolved_nomen"] = data["canonicalName"]
                #     elif "species" in best_match:
                #         best_match["resolved_name"] = best_match["species"]

                elif best_match["status"]!="ACCEPTED":
                    self.logger.warning("weird status: {}".format(best_match["status"]))

            else:
                self.logger.warning("{}: no sanitized name!?".format(self.current_name["raw"]))

            if "key" in best_match["lookup"] and not self.force_canonical:
                best_match["lookup"]["scientificName"] = self.gbif_lookup_sciname_by_key(best_match["lookup"]["key"])

            self.current_name["best_match"] = best_match


    def gbif_lookup_sciname_by_key(self,key):
        p = [x for x in self.scinames_by_key if x["key"]==key ]

        if len(p)>0:
            return p[0]["sciname"]
        else:
            url = self.url_gbif_key_lookup.format(key=key)
            response = urllib.request.urlopen(url)
            data = response.read()
            data = json.loads(data)
            if "scientificName" in data:
                self.scinames_by_key.append({"key":key,"sciname":data["scientificName"]})
                return data["scientificName"]

    def write_names(self):
        with open(self.output_file, 'w', newline='') as csvfile:
            delimiter=","
            writer = csv.writer(csvfile, delimiter=delimiter,quoting=csv.QUOTE_MINIMAL)

            for index, line in enumerate(self.input_lines):
                name = self.names[index]
                if ("best_match" in name) and ("lookup" in name["best_match"]):
                    if "scientificName" in name["best_match"]["lookup"] and not self.force_canonical:
                        value = name["best_match"]["lookup"]["scientificName"]
                    elif "value" in name["best_match"]["lookup"]:
                        value = name["best_match"]["lookup"]["value"]
                else:
                    value = name["raw"]
                row = line[1:]
                row.insert(0,value)
                row.append(line[0])
                writer.writerow(row)

        self.logger.info("wrote output: {}".format(self.output_file))

    def print_skipped_lines(self):
        for item in self.skipped_lines:
            self.logger.info("skipped {record}: {cause}".format(record=item["record"],cause=item["cause"]))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file",type=str,required=True)
    parser.add_argument("--output_file",type=str,required=True)
    parser.add_argument("--minimum_confidence_score",type=int,default=75)
    parser.add_argument("--rank_to_retrieve",type=str,required=True)
    parser.add_argument("--class_col",type=int,default=0)
    parser.add_argument("--force_canonical",action="store_true")

    args = parser.parse_args()

    n = GbifHigherTaxonLookup()

    n.set_input_file(args.input_file)
    n.set_output_file(args.output_file)
    n.set_class_col(args.class_col)
    n.set_rank_to_retrieve(args.rank_to_retrieve)
    n.set_minimum_confidence_score(args.minimum_confidence_score)
    n.set_force_canonical(args.force_canonical)
    n.read_input_file()
    n.preprocess_names()
    n.gbif_lookup_names()
    n.write_names()
    n.print_skipped_lines()

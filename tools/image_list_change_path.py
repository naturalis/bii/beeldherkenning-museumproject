import argparse, csv, os
from lib import logclass

class ImageChangePath():

    image_list_file = None
    output_file = None
    csv_delimiter = None
    image_list_lines = []
    changed_lines = []
    class_col = 0
    image_col = 1

    def __init__(self):
        pr = os.environ.get("PROJECT_ROOT")

        if pr is None:
            raise ValueError("need a project root (PROJECT_ROOT missing from .env)")

        logfile = os.path.join(pr, "log", "general.log")

        if not os.path.exists(logfile):
            path = os.path.join(pr, "log")
            if not os.path.exists(path):
                os.mkdir(path)
                os.chmod(path,0o777)

            with open(logfile, 'w') as fp:
                pass

            os.chmod(logfile,0o777)

        self.logger = logclass.LogClass(self.__class__.__name__,logfile)

    def set_image_list_file(self,image_list_file):
        self.image_list_file = image_list_file
        self.logger.info("image_list_file: {}".format(self.image_list_file))

    def set_output_file(self,output_file):
        self.output_file = output_file
        self.logger.info("output: {}".format(self.output_file))

    def set_csv_delimiter(self,csv_delimiter):
        self.csv_delimiter = "\t" if csv_delimiter =='[tab]' else csv_delimiter
        self.logger.info("csv_delimiter: {}".format(self.csv_delimiter))

    def set_path_prefix(self,path_prefix):
        self.path_prefix = path_prefix
        self.logger.info("path_prefix: {}".format(self.path_prefix))

    def set_path_replace(self,path_replace):
        self.path_replace = path_replace
        self.logger.info("path_replace: {}".format(self.path_replace))

    def read_image_list_file(self):
        with open(self.image_list_file, 'r', encoding='utf-8-sig') as csv_file:
            reader = csv.reader(csv_file, delimiter=self.csv_delimiter)
            self.image_list_lines = list(reader)
        self.logger.info("read {} lines".format(len(self.image_list_lines)))

    def change_path(self):
        for line in self.image_list_lines:
            if self.path_replace:
                line[self.image_col] = os.path.join(self.path_replace,os.path.basename(line[self.image_col]))
            elif self.path_prefix:
                line[self.image_col] = os.path.join(self.path_prefix,line[self.image_col])

            self.changed_lines.append(line)

    def write_output(self):
        with open(self.output_file, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=self.csv_delimiter,quoting=csv.QUOTE_MINIMAL)
            for line in self.changed_lines:
                writer.writerow(line)

        self.logger.info("wrote output: {}".format(self.output_file))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--image_list_file",type=str,required=True)
    parser.add_argument("--output_file",type=str,required=True)
    parser.add_argument("--csv_delimiter", type=str, default=",")
    parser.add_argument("--path_prefix",type=str,help="prefix to relative path in downloaded images list")
    parser.add_argument("--path_replace",type=str,help="prefix to basename of path in downloaded images list")

    args = parser.parse_args()

    n = ImageChangePath()
    n.set_image_list_file(args.image_list_file)
    n.set_output_file(args.output_file)
    n.set_csv_delimiter(args.csv_delimiter)

    if args.path_prefix and args.path_replace:
        raise ValueError("cannot have both path_prefix and path_replace")

    n.set_path_prefix(args.path_prefix)
    n.set_path_replace(args.path_replace)

    n.read_image_list_file()
    n.change_path()
    n.write_output()

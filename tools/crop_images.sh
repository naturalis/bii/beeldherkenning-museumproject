#!/bin/bash

INDIR=$1
OUTDIR=$2

OVERWRITE="n"


echo "script removes right half of all images in the input folder and its subfolders."
echo "not specifying an output folder will cause original images to be overwritten with cropped ones."
echo "only specify an output folder if there are no subfolders in the input, as the subfolder structure is not preserved."
echo "if you need to retain subfolders, first copy the input to a new location and then run this script without specifying output folder."
echo

if [ -z "$INDIR" ]; then
    echo "need a root image folder"
    exit
fi

if [ -z "$OUTDIR" ]; then
    echo "no output folder specified; original images will be overwritten with the cropped versions."
    echo "press 'y' to confirm."
    read OVERWRITE

    if [[ $OVERWRITE != "y" && $OVERWRITE != "Y" ]]; then
        echo "exiting"
        exit
    fi

    OVERWRITE="y"
else
    if [ ! -d $OUTDIR ]; then
        echo "path '$OUTDIR' does not exist"
        exit
    fi
    if [ "$INDIR" = "$OUTDIR" ]; then
        echo "input and output folder are the same (to overwrite, omit output folder)"
        exit
    fi
fi


IFS=$'\n'
for f in $(find $INDIR -type f -name '*.jpg');
do
    BASENAME=$(basename "$f")
    if [[ $OVERWRITE == "y" ]]; then
        mv $f ./tmp
    else
        cp $f ./tmp
    fi

    DIM=$(convert ./tmp -format "%hx%w" info:)
    HEIGHT=$(echo $DIM | sed 's/x[0-9]*$//')
    WIDTH=$(echo $DIM | sed 's/^[0-9]*x//')
    NEW_WIDTH=$(($WIDTH/2))
    convert ./tmp -crop ${NEW_WIDTH}x${HEIGHT}+0+0 +repage ./tmp

    if [[ $OVERWRITE == "y" ]]; then
        mv ./tmp $f
    else
        mv ./tmp $OUTDIR/$BASENAME
    fi

    echo $f
done


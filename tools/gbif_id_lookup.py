import argparse, csv, json, re, os
import urllib.parse, urllib.request
from lib import logclass


class GbifIdLookup():

    input_file = None
    output_file = None
    input_lines = []
    results = []
    id_col = 0
    current_id = None

    url_gbif_occurrence_lookup = 'https://api.gbif.org/v1/occurrence/{key}'

    def __init__(self):
        pr = os.environ.get("PROJECT_ROOT")

        if pr is None:
            raise ValueError("need a project root (PROJECT_ROOT missing from .env)")

        logfile = os.path.join(pr, "log", "general.log")

        if not os.path.exists(logfile):
            path = os.path.join(pr, "log")
            if not os.path.exists(path):
                os.mkdir(path)
                os.chmod(path,0o777)

            with open(logfile, 'w') as fp:
                pass

            os.chmod(logfile,0o777)

        self.logger = logclass.LogClass(self.__class__.__name__,logfile)

        # self.accepted_ranks = self.default_ranks

        # self.logger.info("minimum_confidence_score (default): {}".format(self.minimum_confidence_score))
        # self.logger.info("accepted_ranks (default): {}".format(";".join(self.accepted_ranks)))
        # self.logger.info("full_report_unique_lines_only (default): {}".format(self.full_report_unique_lines_only))
        # self.logger.info("accepted_ranks (default): {}".format(self.accepted_ranks))

    def set_input_file(self,input_file):
        self.input_file = input_file
        self.logger.info("input: {}".format(self.input_file))

        file, extension = os.path.splitext(self.input_file)
        if extension.lower() == ".tsv":
            self.delimiter = "\t"
        else:
            self.delimiter = ","

        self.logger.info("field delimiter (from extension): {}".format("[tab]" if self.delimiter=="\t" else self.delimiter))


    def set_output_file(self,output_file):
        self.output_file = output_file
        self.logger.info("output: {}".format(self.output_file))

    def read_input_file(self):
        with open(self.input_file, 'r', encoding='utf-8-sig') as csvfile:
            reader = csv.reader(csvfile,delimiter=self.delimiter)
            self.input_lines = list(reader)

        self.logger.info("read {} lines".format(len(self.input_lines)))

    def gbif_lookup_ids(self):
        for input_line in self.input_lines:
            self.current_id = input_line[self.id_col]
            self.gbif_lookup_id()
            self.results.append(self.current_result)
            self.logger.info("looked up {} --> {}".format(
                self.current_id,
                self.current_result["scientificName"] if "scientificName" in self.current_result else "-"))

        # print(self.results)

    def gbif_lookup_id(self):
        self.current_result = {}

        if not self.current_id.isnumeric():
            self.logger.info("skipping ID: {} ".format(self.current_id))
            return

        url = self.url_gbif_occurrence_lookup.format(key=urllib.parse.quote(self.current_id))

        response = urllib.request.urlopen(url)
        data = response.read()
        data = json.loads(data)

        self.current_result = {
            "scientificName" : data["scientificName"],
            "acceptedScientificName" : data["acceptedScientificName"],
            "taxonRank" : data["taxonRank"],
            # "taxonomicStatus" : data["taxonomicStatus"]
        }

    def write_names(self):
        with open(self.output_file, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=self.delimiter,quoting=csv.QUOTE_MINIMAL)
            for index, line in enumerate(self.input_lines):
                names = self.results[index]
                name = names["acceptedScientificName"] if "acceptedScientificName" in names else "-"
                row = line[1:]
                row.insert(0,name)
                row.append(line[0])
                writer.writerow(row)

        self.logger.info("wrote output: {}".format(self.output_file))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file",type=str,required=True,help="CSV file with GBIF occurrence ID's (in first column), one per line")
    parser.add_argument("--output_file",type=str,required=True)
    args = parser.parse_args()

    n = GbifIdLookup()
    n.set_input_file(args.input_file)
    n.set_output_file(args.output_file)

    n.read_input_file()
    n.gbif_lookup_ids()
    n.write_names()

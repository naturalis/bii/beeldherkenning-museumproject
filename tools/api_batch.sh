#!/bin/bash

IMAGE_FOLDER=$1
MODEL_SLUG=$2

API_BASE_URL=https://museum.identify.biodiversityanalysis.nl/api/
TOP_PREDICTIONS=3
FIELD_SEP=$(echo -e ' \t ')

if [[ -z "$IMAGE_FOLDER" ]]; then
    echo "need an image folder"
    exit
fi

if [[ -z "$MODEL_SLUG" ]]; then
    echo "need a model selector"
    exit
fi

OUT=$(curl -s -L $API_BASE_URL$MODEL_SLUG/classes)
ERROR=$(echo $OUT | jq '.error')

if [[ "$ERROR" != "null" ]]; then
    echo $ERROR
    echo "available models:"
    OUT=$(curl -s -L $API_BASE_URL/models)
    echo $OUT | jq
    exit
fi

COUNTER=0
echo -n "FILENAME"
for OUTPUT in $(seq $TOP_PREDICTIONS)
do
    echo -n "${FIELD_SEP}CLASS ${COUNTER}${FIELD_SEP}PRED ${COUNTER}"
    let COUNTER++
done
echo


for item in $(ls $IMAGE_FOLDER/*.jpg); do
    BASENAME=$(basename $item)
    OUT=$(curl -s -L -XPOST -F "image=@${item}" ${API_BASE_URL}${MODEL_SLUG}/identify)
    PREDS=$(echo $OUT | jq '.predictions')
    COUNTER=0
    echo -n ${BASENAME}
    for OUTPUT in $(seq $TOP_PREDICTIONS)
    do
        CLASS=$(echo $PREDS | jq ".[${COUNTER}].class")
        PRED=$(echo $PREDS | jq ".[${COUNTER}].prediction")
        echo -n "${FIELD_SEP}${CLASS}${FIELD_SEP}${PRED}"
        let COUNTER++
    done
    echo
done

import argparse, csv, os
from lib import logclass

class NameTranslator():

    input_file = None
    translator_file = None
    output_file = None
    input_lines = []
    translator = []
    class_col = 0
    csv_delimiter = ","

    def __init__(self):
        pr = os.environ.get("PROJECT_ROOT")

        if pr is None:
            raise ValueError("need a project root (PROJECT_ROOT missing from .env)")

        logfile = os.path.join(pr, "log", "general.log")

        if not os.path.exists(logfile):
            path = os.path.join(pr, "log")
            if not os.path.exists(path):
                os.mkdir(path)
                os.chmod(path,0o777)

            with open(logfile, 'w') as fp:
                pass

            os.chmod(logfile,0o777)

        self.logger = logclass.LogClass(self.__class__.__name__,logfile)

    def set_input_file(self,input_file):
        self.input_file = input_file
        self.logger.info("input_file: {}".format(self.input_file))

    def set_translator_file(self,translator_file):
        self.translator_file = translator_file
        self.logger.info("translator_file: {}".format(self.translator_file))


    def set_output_file(self,output_file):
        self.output_file = output_file
        self.logger.info("output_file: {}".format(self.output_file))

    def set_csv_delimiter(self,csv_delimiter):
        self.csv_delimiter = "\t" if csv_delimiter =='[tab]' else csv_delimiter
        self.logger.info("csv_delimiter: {}".format(self.csv_delimiter))

    def read_input_file(self):
        with open(self.input_file, 'r', encoding='utf-8-sig') as csv_file:
            reader = csv.reader(csv_file, delimiter=self.csv_delimiter)
            self.input_lines = list(reader)

        self.names = [ { "raw" : x[self.class_col] } for x in self.input_lines]
        self.logger.info("read {} names".format(len(self.names)))

    def read_translator_file(self):
        with open(self.translator_file, 'r', encoding='utf-8-sig') as csv_file:
            reader = csv.reader(csv_file, delimiter=self.csv_delimiter)
            self.translator = list(reader)

        self.logger.info("read {} translator lines".format(len(self.translator)))

    def translate_names(self):
        for name in self.names:
            p = [x for x in self.translator if x[0]==name["raw"]]
            if (p):
                name["translation"]=p[0][1]
                self.logger.info("{} --> {}".format(name["raw"],name["translation"]))
            else:
                self.logger.info("{} --> (no translation)".format(name["raw"]))

    def write_names(self):
        with open(self.output_file, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=self.csv_delimiter,quoting=csv.QUOTE_MINIMAL)

            for index, line in enumerate(self.input_lines):
                row = line[1:]
                if "translation" in self.names[index]:
                    row.insert(0,self.names[index]["translation"] if "translation" in self.names[index] else "?")
                    row.append(line[0])
                else:
                    row.insert(0,line[0])
                    row.append("-")

                writer.writerow(row)

        self.logger.info("wrote output: {}".format(self.output_file))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file",type=str,required=True)
    parser.add_argument("--translator_file",type=str,required=True,help="col 0: original, col 1: translation")
    parser.add_argument("--output_file",type=str,required=True)
    parser.add_argument("--csv_delimiter", type=str, default=",")
    args = parser.parse_args()

    n = NameTranslator()
    n.set_input_file(args.input_file)
    n.set_translator_file(args.translator_file)
    n.set_output_file(args.output_file)
    n.set_csv_delimiter(args.csv_delimiter)

    n.read_input_file()
    n.read_translator_file()
    n.translate_names()
    n.write_names()

import pandas as pd


def _csv_to_dataframe(filepath, encoding="utf-8-sig"):
    return pd.read_csv(
        filepath,
        encoding=encoding,
        sep=",",
        dtype="str", header=None)

class_list = _csv_to_dataframe("classes.csv")

print(class_list)
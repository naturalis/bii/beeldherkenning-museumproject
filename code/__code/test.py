import tensorflow as tf
import os
tf.test.gpu_device_name()

from glob import glob
from sklearn.model_selection import train_test_split




ROOT_DIR = '/data/alien/images'

# cats = glob(os.path.join(ROOT_DIR,'train/alien/*.jpg'))
# dogs = glob(os.path.join(ROOT_DIR,'train/predator/*.jpg'))

# cats_train, cats_test = train_test_split(cats, test_size=0.30)
# dogs_train, dogs_test = train_test_split(dogs, test_size=0.30)

TRAIN_DIR = os.path.join(ROOT_DIR,'train')
TEST_DIR = os.path.join(ROOT_DIR,'test')

# os.mkdir(TEST_DIR)
# os.mkdir(os.path.join(TEST_DIR,'alien'))
# os.mkdir(os.path.join(TEST_DIR,'predator'))


# for file in cats_test:
#     os.rename(file, "path/to/new/destination/for/file.foo")


# files = ' '.join()

# !mv -t test/alien $files

# !mkdir test/predator
# files = ' '.join(dogs_test)
# !mv -t test/predator $files

# exit()


# from keras.models import Model
# from keras.layers import Dense, GlobalAveragePooling2D, Dropout
# from keras.applications.inception_v3 import InceptionV3, preprocess_input

CLASSES = 2

# setup model
# base_model = InceptionV3(weights='imagenet', include_top=False)

base_model = tf.keras.applications.InceptionV3(weights='imagenet', include_top=False)

x = base_model.output
x = tf.keras.layers.GlobalAveragePooling2D(name='avg_pool')(x)
x = tf.keras.layers.Dropout(0.4)(x)
predictions = tf.keras.layers.Dense(CLASSES, activation='softmax')(x)
model = tf.keras.models.Model(inputs=base_model.input, outputs=predictions)

# transfer learning
for layer in base_model.layers:
    layer.trainable = False

model.compile(optimizer='rmsprop',
              loss='categorical_crossentropy',
              metrics=['accuracy'])


# from keras.preprocessing.image import ImageDataGenerator

WIDTH = 299
HEIGHT = 299
BATCH_SIZE = 32

# data prep
train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    preprocessing_function=tf.keras.applications.inception_v3.preprocess_input,
    rotation_range=40,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest')

validation_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    preprocessing_function=tf.keras.applications.inception_v3.preprocess_input,
    rotation_range=40,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest')

train_generator = train_datagen.flow_from_directory(
    TRAIN_DIR,
    target_size=(HEIGHT, WIDTH),
        batch_size=BATCH_SIZE,
        class_mode='categorical')

validation_generator = validation_datagen.flow_from_directory(
    TEST_DIR,
    target_size=(HEIGHT, WIDTH),
    batch_size=BATCH_SIZE,
    class_mode='categorical')


# EPOCHS = ["EPOCHS"] if "EPOCHS" in os.environ else 50

EPOCHS = 50
# STEPS_PER_EPOCH = 320
# VALIDATION_STEPS = 64
STEPS_PER_EPOCH = train_generator.n // train_generator.batch_size
VALIDATION_STEPS = validation_generator.n // validation_generator.batch_size



trainable_count = np.sum([tf.keras.backend.count_params(w) for w in model.trainable_weights])
non_trainable_count = np.sum([tf.keras.backend.count_params(w) for w in model.non_trainable_weights])

params =  {
    "total" : trainable_count + non_trainable_count,
    "trainable" : trainable_count,
    "non_trainable" : non_trainable_count
}

print("trainable variables: {:,}".format(len(model.trainable_variables)))
print("total parameters: {:,}".format(params["total"]))
print("trainable: {:,}".format(params["trainable"]))
print("non-trainable: {:,}".format(params["non_trainable"]))


history = model.fit_generator(
    train_generator,
    epochs=EPOCHS,
    steps_per_epoch=STEPS_PER_EPOCH,
    validation_data=validation_generator,
    validation_steps=VALIDATION_STEPS)



# MODEL_FILE = 'filename.model'
# model.save(MODEL_FILE)


# Please use Model.fit, which supports generators.

# 0.9688


# 2021-09-13 14:12:23,711 - ModelTrainer - INFO - === training phase 1/1 ===
# 2021-09-13 14:12:23,785 - ModelTrainer - INFO - frozen layers: base_model
# 2021-09-13 14:12:23,799 - ModelTrainer - INFO - trainable variables: 2
# 2021-09-13 14:12:23,799 - ModelTrainer - INFO - total parameters: 21,806,882
# 2021-09-13 14:12:23,800 - ModelTrainer - INFO - trainable: 4,098
# 2021-09-13 14:12:23,800 - ModelTrainer - INFO - non-trainable: 21,802,784


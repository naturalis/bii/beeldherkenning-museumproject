import argparse, csv
# import collections
from lib import baseclass


class CombineImageLists(baseclass.BaseClass):

#     valid_extensions = [ ".jpg", ".jpeg" ]
#     valid_extensions_convert = [ ".png", ".gif" ]

#     out = []
#     classes = []

#     contains_convertables = False

    image_list_files = []
    active_file = None

    def __init__(self):
        super().__init__()

    def set_image_list_file(self,file):
        if os.path.exists(file):
            self.image_list_files.append(file)
            self.logger.info("added {}".format(file))
        else:
            self.logger.error("file doesn't exist: {}".format(file))

    def combine(self):
        for file in self.image_list_files:
            self.active_file = file
            self.read_file()

    def read_file(self):
        separator = utils._determine_csv_delimiter(self.active_file,"utf-8-sig")

        with open(self.active_file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=separator)
            for row in csv_reader:






#     def traverse_image_root(self):
#         for root, dirs, files in os.walk(self.image_root_path):
#             path = root.split(os.sep)
#             for file in files:
#                 filename, file_extension = os.path.splitext(file)
#                 if file_extension.lower() in self.valid_extensions or file_extension.lower() in self.valid_extensions_convert:
#                     classname = root.replace(self.image_root_path,"").replace("/","")
#                     self.out.append([classname, os.path.join(classname,file)])
#                     self.classes.append(classname)
#                     if file_extension.lower() in self.valid_extensions_convert:
#                         self.contains_convertables = True
#                 else:
#                     self.logger.info("skipped {}".format(file_extension))

#         self.freq=collections.Counter(self.classes)
#         self.logger.info("found {} images".format(len(self.out)))
#         self.logger.info("found {} classes".format(len(self.freq)))
#         if self.contains_convertables:
#             self.logger.warning("some images must be converted; run image_convert")

#     def write_lists(self):
#         with open(self.downloaded_images_file,'w') as csvfile:
#             s = csv.writer(csvfile)
#             for line in self.out:
#                 s.writerow(line)

#         with open(self.class_list_file_csv,'w') as csvfile:
#             s = csv.writer(csvfile)
#             for item in self.freq:
#                 s.writerow([item,self.freq[item]])

#         print("wrote {}".format(self.downloaded_images_file))
#         print("wrote {}".format(self.class_list_file_csv))



if __name__ == "__main__":


    parser = argparse.ArgumentParser()
    parser.add_argument("--image_lists", type=str, help='comma separated list of image list files to be combined')
    args = parser.parse_args()

    if args.image_lists:

        c = CombineImageLists()
        c.set_debug(os.environ["DEBUG"]=="1" if "DEBUG" in os.environ else False)
        c.set_project(os.environ)

        for file in args.image_lists.split(","):
            c.set_image_list_file(file)

        c.combine()



import os, time, csv
import ssl, requests
from urllib3 import poolmanager
from PIL import Image

# because csv sniffer doesn't always work
def _determine_csv_delimiter(filepath,encoding):
    f = open(filepath, "r", encoding=encoding)
    line = f.readline()

    tab = line.count('\t')
    comma = line.count(',')
    semi = line.count(';')

    if (tab > comma) and (tab > semi):
        sep = '\t'
    elif semi > comma:
        sep = ';'
    else:
        sep = ','

    return sep

class Timer:

    start_time = None
    end_time = None
    formats = {
        "pretty" : "%02dd %02dh %02dm %02ds"
    }
    milestones = []

    def __init__(self):
        self.set_start_time()

    def set_start_time(self):
        self.start_time = self.get_timestamp()

    def set_end_time(self):
        self.end_time = self.get_timestamp()

    def get_timestamp(self):
        return time.time()

    def add_milestone(self,label):
        self.milestones.append({ "label" : label, "timestamp" : self.get_timestamp() })

    def get_milestones(self):
        return self.milestones

    def reset_milestones(self):
        self.milestones = []

    def get_time_passed(self,format="pretty"):
        if None is self.end_time:
            self.set_end_time()
        time = float(self.end_time - self.start_time)
        day = time // (24 * 3600)
        time = time % (24 * 3600)
        hour = time // 3600
        time %= 3600
        minutes = time // 60
        time %= 60
        seconds = time
        return self.formats[format] % (day, hour, minutes, seconds)

class TLSAdapter(requests.adapters.HTTPAdapter):

    def init_poolmanager(self, connections, maxsize, block=False):
        """Create and initialize the urllib3 PoolManager."""
        ctx = ssl.create_default_context()
        ctx.set_ciphers('DEFAULT@SECLEVEL=1')
        self.poolmanager = poolmanager.PoolManager(
                num_pools=connections,
                maxsize=maxsize,
                block=block,
                ssl_version=ssl.PROTOCOL_TLS,
                ssl_context=ctx)

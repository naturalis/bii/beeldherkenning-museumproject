import json, argparse, requests
import os, csv, re
from urllib.parse import urlparse
from hashlib import md5
from datetime import datetime
# import urllib.request
from lib import baseclass
from lib import utils

class ImageDownloader(baseclass.BaseClass):
    image_list_file = None
    image_list = []
    previously_downloaded_files = []
    skip_download_if_exists = True
    image_default_extension = None
    image_url_to_name = None
    subfolder_max_files = 1000
    _subfolder_index = 0
    _current_subdir = None
    override_download_folder = None
    override_output_file = None
    image_col = 1
    class_col = 0
    classes = []
    request_timeout = 10
    max_failed_downloads_ratio = 0.5
    failed_lines = []
    path_prefix = ""
    include_original_data = False
    no_session = False
    headers = {
        'User-Agent': 'ImageDownloader/0.1'
    }

    def __init__(self):
        super().__init__()
        self.logger.info("subfolder_max_files (default): {}".format(self.subfolder_max_files))
        self.logger.info("skip_download_if_exists (default): {}".format(self.skip_download_if_exists))

    def set_list_class_column(self,class_col):
        self.class_col = int(class_col)
        self.logger.info("class_col: {}".format(self.class_col))

    def set_list_file_column(self,image_col):
        self.image_col = int(image_col)
        self.logger.info("image_col: {}".format(self.image_col))

    def set_image_url_to_name(self, image_url_to_name):
        self.image_url_to_name = image_url_to_name

    def set_image_list_file(self, image_list_file=None):
        if image_list_file is not None:
            self.image_list_file = image_list_file
        else:
            self.image_list_file = os.path.join(self.project_root, 'lists', 'images.csv')

        if not os.path.isfile(self.image_list_file):
            raise FileNotFoundError("image list file not found: {}".format(self.image_list_file))

        self.logger.info("image list file: {}".format(self.image_list_file))

    def set_image_list_delimiter(self,image_list_delimiter):
        self.image_list_delimiter = "\t" if image_list_delimiter=="tab" else image_list_delimiter
        self.logger.info("image list file delimiter: {}".format("[tab]" if self.image_list_delimiter=="\t" else self.image_list_delimiter))

    def set_subfolder_max_files(self, max_files):
        if isinstance(max_files, int) and 100 < int < 100000:
            self.subfolder_max_files = max_files
        else:
            raise ValueError("max subfolder files must be int between 100 and 100000 ({})".format(max_files))

    def set_skip_download_if_exists(self,state):
        if isinstance(state, bool):
            self.skip_download_if_exists = state
            self.logger.info("set skip download if exists: {}".format(self.skip_download_if_exists))
        else:
            raise ValueError("skip download if exists must be a boolean ({})".format(state))

    def set_override_download_folder(self, folder):
        if not os.path.exists(folder):
            raise FileNotFoundError("folder doesn't exist: {}".format(folder))

        if not os.access(folder, os.W_OK):
            raise FileNotFoundError("folder not writeable: {}".format(folder))

        self.override_download_folder = folder
        self.logger.info("using manual override image folder: {}".format(self.override_download_folder))

    def set_override_output_file(self, override_output_file):
        self.override_output_file = override_output_file
        self.logger.info("using override output file: {}".format(self.override_output_file))

    def set_request_timeout(self, request_timeout):
        self.request_timeout = request_timeout
        self.logger.info("request timeout: {}s".format(self.request_timeout))

    def set_path_prefix(self, path_prefix):
        self.path_prefix = path_prefix
        self.logger.info("path_prefix: {}".format(self.path_prefix))

    def set_include_original_data(self,include_original_data):
        self.include_original_data = include_original_data
        self.logger.info("include original data: {}".format(self.include_original_data))

    def set_no_session(self,no_session):
        self.no_session = no_session
        self.logger.info("no TLS session: {}".format(self.no_session))

    def read_image_list(self):
        with open(self.image_list_file, 'r', encoding='utf-8-sig') as file:
            c = csv.reader(file,delimiter=self.image_list_delimiter)
            for row in c:
                self.image_list.append(row)

    def get_previously_downloaded_files(self):
        if not self.skip_download_if_exists:
            return

        if not self.override_download_folder is None:
            root_folder = self.override_download_folder
        else:
            root_folder = self.image_root_path

        for subdir, dirs, files in os.walk(root_folder):
            for file in files:
                self.previously_downloaded_files.append( \
                {"file": file, "path": os.path.join(subdir.replace(root_folder,""),file).lstrip("/")})

    def count_class(self,this_class):
        for idx, item in enumerate(self.classes):
           if item["class"]==this_class:
               item["count"] = item["count"]+1
               self.classes[idx] = item
               return

        self.classes.append({"class":this_class,"count":1})

    def check_name_conversion(self):
        for item in self.image_list:
            if len(item)<2:
                continue
            url = item[self.image_col]
            break

        p = urlparse(url)

        if self.image_url_to_name is not None and "expression" in self.image_url_to_name and "replace" in self.image_url_to_name:
            filename = re.sub(self.image_url_to_name["expression"], self.image_url_to_name["replace"], p.path)
        else:
            filename = os.path.basename(p.path)

        if self.image_url_to_name is not None and "postfix" in self.image_url_to_name:
            filename += self.image_url_to_name["postfix"]

        # if filename.find("/")!=-1:
        self.logger.warning("local file basename will look like this: {}".format(filename))
        proceed = "a"
        while not proceed in ["n","y","N","Y"]:
            proceed = input("proceed? [y/n] ")

        if proceed in ["n","N"]:
            self.logger.info("check value for IMAGE_URL_TO_NAME; currently: {}".format(os.environ['IMAGE_URL_TO_NAME']))
            self.logger.info("exiting")
            exit(0)

    def download_images(self):
        downloaded = 0
        skipped = 0
        self.failed_lines = []

        if not self.override_output_file is None:
            outfile = self.override_output_file
        else:
            outfile = self.downloaded_images_file

        if not self.no_session:
            # initiate persistent session
            session = requests.session()
            session.mount('https://', utils.TLSAdapter())

        with open(outfile, 'w') as csvfile:
            c = csv.writer(csvfile, delimiter=',', quotechar='"')

            for item in self.image_list:
                if len(item)<2:
                    continue

                url = item[self.image_col]
                this_class = item[self.class_col]

                if self.override_download_folder is None:
                    self.set_download_subdir()


                p = urlparse(url)

                if self.image_url_to_name is not None and "expression" in self.image_url_to_name and "replace" in self.image_url_to_name:
                    filename = re.sub(self.image_url_to_name["expression"], self.image_url_to_name["replace"], p.path)
                else:
                    filename = os.path.basename(p.path)

                if self.image_url_to_name is not None and "postfix" in self.image_url_to_name:
                    filename += self.image_url_to_name["postfix"]

                # print(item)
                # print(url)
                # print(p)
                # print(filename)

                if self.skip_download_if_exists:
                    existing_images = [x for x in self.previously_downloaded_files if x["file"] == filename]
                    skip_download = len(existing_images) > 0
                else:
                    skip_download = False

                if skip_download:
                    # c.writerow([this_class, existing_images[0]["path"], url])
                    out = [this_class,os.path.join(self.path_prefix,existing_images[0]["path"])]

                    if self.include_original_data:
                        for idx, cell in enumerate(item):
                            if idx < 1:
                                continue
                            out.append(cell)

                    c.writerow(out)

                    self.logger.info("skipped (file exists): {}".format(url))
                    skipped += 1

                    self.count_class(this_class)

                else:

                    if not self.override_download_folder is None:
                        file_to_save = os.path.join(self.override_download_folder, filename)
                        subdir_to_write = self.override_download_folder
                        # print(self.override_download_folder)
                        # print(filename)
                        # print(file_to_save)

                    else:
                        file_to_save = os.path.join(self.image_root_path, self._current_subdir,filename)
                        subdir_to_write = self._current_subdir

                    try:
                        # urllib.request.urlretrieve(url, file_to_save)
                        # r = requests.get(url,allow_redirects=True,timeout=10)
                        # r = requests.get(url,allow_redirects=True,timeout=10,verify=False)
                        if not self.no_session:
                            r = session.get(url,allow_redirects=True,timeout=self.request_timeout,headers=self.headers)
                        else:
                            r = requests.get(url,allow_redirects=True,timeout=self.request_timeout,headers=self.headers)

                        if len(r.content)>0:
                            open(file_to_save, 'wb').write(r.content)
                            # c.writerow([this_class, os.path.join(subdir_to_write, filename), url])

                            out = [this_class, os.path.join(self.path_prefix,subdir_to_write,filename)]

                            if self.include_original_data:
                                for idx, cell in enumerate(item):
                                    if idx < 1:
                                        continue
                                    out.append(cell)

                            c.writerow(out)

                            self.logger.info("downloaded {} to {} ({}kb)".format(url, os.path.basename(file_to_save), round(os.path.getsize(file_to_save)/1000)))
                            downloaded += 1
                            self.count_class(this_class)
                        else:
                            self.logger.info("skipped (0 byte file): {}".format(url))
                            skipped += 1

                    except Exception as e:
                        self.logger.error("could not download {}: {}".format(url, e))
                        self.failed_lines.append(item)

                failed = len(self.failed_lines)
                succes = (downloaded+skipped)

                if (failed > 10) and (succes==0 or ((failed / succes) > self.max_failed_downloads_ratio)):
                    raise ValueError("failed/downloaded ratio exceeds {}; exiting".format(self.max_failed_downloads_ratio))

        self.logger.info("downloaded {}, skipped {}, failed {}".format(downloaded, skipped, len(self.failed_lines)))

    def set_download_subdir(self):
        self._current_subdir = md5(str(self._subfolder_index).encode('utf-8')).hexdigest()[:10]
        subdir_path = os.path.join(self.image_root_path, self._current_subdir)
        if not os.path.isdir(subdir_path):
            os.mkdir(subdir_path)

        n = len([name for name in os.listdir(subdir_path) if os.path.isfile(os.path.join(subdir_path, name))])
        if n >= self.subfolder_max_files:
            while True:
                self._subfolder_index += 1
                self._current_subdir = md5(str(self._subfolder_index).encode('utf-8')).hexdigest()[:10]
                if not os.path.isdir(os.path.join(self.image_root_path, self._current_subdir)):
                    os.mkdir(os.path.join(self.image_root_path, self._current_subdir))
                    break

    def write_failed_lines(self):
        if len(self.failed_lines)==0:
            return

        self.failed_file = os.path.join(self.project_root, 'lists', 'failed_downloaded_images--{}.csv'.format(datetime.now().strftime('%Y%m%d%H%M%S')))

        with open(self.failed_file, 'w') as csvfile:
            c = csv.writer(csvfile, delimiter=',', quotechar='"')
            for line in self.failed_lines:
                c.writerow(line)

        self.logger.info("wrote failed lines to {}".format(self.failed_file))


if __name__ == "__main__":

    downloader = ImageDownloader()
    downloader.set_project(os.environ)

    if 'IMAGE_URL_TO_NAME' in os.environ:
        downloader.set_image_url_to_name(json.loads(os.environ['IMAGE_URL_TO_NAME']))

    if 'IMAGE_LIST_CLASS_COLUMN' in os.environ:
        downloader.set_list_class_column(json.loads(os.environ['IMAGE_LIST_CLASS_COLUMN']))

    if 'IMAGE_LIST_FILE_COLUMN' in os.environ:
        downloader.set_list_file_column(json.loads(os.environ['IMAGE_LIST_FILE_COLUMN']))

    parser = argparse.ArgumentParser()
    parser.add_argument("--override_image_list",type=str,help="alternative input image list")
    parser.add_argument("--override_download_folder",type=str,help="alternative download folder")
    parser.add_argument("--override_output_file",type=str,help="alternative output downloaded images list")
    parser.add_argument("--path_prefix",type=str,help="prefix to relative path in downloaded images list")
    parser.add_argument("--image_list_delimiter",type=str,default=",",help="use 'tab' for tab")
    parser.add_argument("--request_timeout",type=int,default=10)
    parser.add_argument("--include_original_data",action="store_true")
    parser.add_argument("--no_session",action="store_true")
    args = parser.parse_args()

    downloader.set_image_list_file(args.override_image_list)
    downloader.set_image_list_delimiter(args.image_list_delimiter)

    if args.override_download_folder:
        downloader.set_override_download_folder(args.override_download_folder)

    if args.override_output_file:
        downloader.set_override_output_file(args.override_output_file)

    if args.path_prefix:
        downloader.set_path_prefix(args.path_prefix)

    downloader.set_request_timeout(args.request_timeout)
    downloader.set_include_original_data(args.include_original_data)
    downloader.set_no_session(args.no_session)
    downloader.read_image_list()
    downloader.get_previously_downloaded_files()
    downloader.check_name_conversion()
    downloader.download_images()
    downloader.write_failed_lines()

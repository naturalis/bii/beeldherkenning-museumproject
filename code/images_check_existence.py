import os, argparse
from lib import utils, dataset

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--alt_image_list",type=str,help="specify alternative downloaded image list")
    args = parser.parse_args()

    dataset = dataset.DataSet()
    dataset.set_environ(os.environ)
    dataset.set_debug(os.environ["DEBUG"]=="1" if "DEBUG" in os.environ else False)
    dataset.set_project(os.environ)
    dataset.set_presets(os_environ=os.environ)

    if args.alt_image_list:
        dataset.set_alt_downloaded_images_file(args.alt_image_list)

    # class_col=dataset.get_preset("image_list_class_column")
    # image_col=dataset.get_preset("image_list_file_column")

    dataset.read_image_list_file(class_col=0,image_col=1,use_original_image_file=True)
    dataset.test_image_existence()

import os, csv, argparse
import collections
from lib import baseclass


class ImagesFromClassFolders(baseclass.BaseClass):

    valid_extensions = [ ".jpg", ".jpeg" ]
    valid_extensions_convert = [ ".png", ".gif" ]
    override_image_folder = None

    out = []
    classes = []

    contains_convertables = False
    image_root_folder = None
    imagelist_file = None
    imagelist_path_prefix = ""

    def __init__(self):
        super().__init__()
        self.logger.info("valid extensions: {}".format(self.valid_extensions))
        self.logger.info("valid extensions (if converted): {}".format(self.valid_extensions_convert))

    def set_image_root_folder(self,image_root_folder):
        if not os.path.exists(image_root_folder):
            raise FileNotFoundError("folder doesn't exist: {}".format(image_root_folder))

        self.image_root_folder = image_root_folder
        self.logger.info("using image root folder: {}".format(self.image_root_folder))

    def set_imagelist_file(self,imagelist_file):
        self.imagelist_file = imagelist_file
        self.logger.info("images file: {}".format(self.imagelist_file))

    def set_imagelist_path_prefix(self,imagelist_path_prefix):
        self.imagelist_path_prefix = imagelist_path_prefix
        self.logger.info("imagelist path prefix: {}".format(self.imagelist_path_prefix))

    def traverse_image_folder(self):
        for root, dirs, files in os.walk(self.image_root_folder):
            path = root.split(os.sep)
            for file in files:
                filename, file_extension = os.path.splitext(file)
                if file_extension.lower() in self.valid_extensions or file_extension.lower() in self.valid_extensions_convert:
                    classname = root.replace(self.image_root_folder,"").replace("/","")
                    self.out.append([classname, os.path.join(self.imagelist_path_prefix,classname,file)])
                    self.classes.append(classname)
                    if file_extension.lower() in self.valid_extensions_convert:
                        self.contains_convertables = True
                else:
                    self.logger.info("skipped {}".format(file_extension))

        self.freq=collections.Counter(self.classes)
        self.logger.info("found {} images".format(len(self.out)))
        self.logger.info("found {} classes".format(len(self.freq)))

        if self.contains_convertables:
            self.logger.warning("some images must be converted; run image_convert")

    def write_downloaded_images_file(self):
        with open(self.imagelist_file,'w') as csvfile:
            s = csv.writer(csvfile)
            for line in self.out:
                s.writerow(line)

        self.logger.info("wrote {}".format(self.imagelist_file))


if __name__ == "__main__":

    ifcf = ImagesFromClassFolders()
    ifcf.set_debug(os.environ["DEBUG"]=="1" if "DEBUG" in os.environ else False)
    ifcf.set_project(os.environ)

    parser = argparse.ArgumentParser()
    parser.add_argument("--image_root_folder",type=str,required=True)
    parser.add_argument("--imagelist_file",type=str,required=True)
    parser.add_argument("--imagelist_path_prefix",type=str)
    args = parser.parse_args()

    ifcf.set_image_root_folder(args.image_root_folder)
    ifcf.set_imagelist_file(args.imagelist_file)
    if args.imagelist_path_prefix:
        ifcf.set_imagelist_path_prefix(args.imagelist_path_prefix)
    ifcf.traverse_image_folder()
    ifcf.write_downloaded_images_file()



import csv, argparse, json, os
from lib import logclass
from lib import baseclass, utils

class DwcaReader(baseclass.BaseClass):
    """
  DwCA reader:
  - extracts an imagelist from the occurence file from a DwCA archive
  """

    override_dwca_folder = None
    override_image_list = None
    dwca_folder = None
    dwca_occurrence_file = None
    dwca_multimedia_file = None
    has_multimedia_extension = True
    multimedia_files = []
    records = []
    occurrence_file_name = 'occurrence.txt'
    multimedia_file_name = 'multimedia.txt'
    occurrence_column_headers = {"taxon": "scientificName", "images": "associatedMedia","gbifID": "gbifID"}
    occurrence_column_indices = {"taxon": None, "images": None, "gbifID": None}
    multimedia_column_headers = {"image": "identifier", "gbifID": "gbifID"}
    multimedia_column_indices = {"image": None, "gbifID": None}

    classification_classes = []
    total_images = 0

    def __init__(self):
        super().__init__()

    def set_override_dwca_folder(self,override_dwca_folder):
        if override_dwca_folder is not None:
            self.override_dwca_folder = override_dwca_folder
            self.logger.info("set alt. DwCA folder: {}".format(self.override_dwca_folder))

    def set_override_image_list(self,override_image_list):
        if override_image_list is not None:
            self.override_image_list = override_image_list
            self.logger.info("set alt. image list: {}".format(self.override_image_list))

    def set_dwca_folder(self):
        if self.override_dwca_folder is not None:
            self.dwca_folder = self.override_dwca_folder
        else:
            self.dwca_folder = os.path.join(self.project_root, 'dwca')

        if not os.path.exists(self.dwca_folder):
            raise FileNotFoundError("DWCA folder not found: {}".format(self.dwca_folder))

        self.logger.info("reading from DwCA path: {}".format(self.dwca_folder))

    def set_dwca_files(self):
        self.dwca_occurrence_file = os.path.join(self.dwca_folder, self.occurrence_file_name)
        self.dwca_multimedia_file = os.path.join(self.dwca_folder, self.multimedia_file_name)

        if not os.path.exists(self.dwca_occurrence_file):
            raise FileNotFoundError("occurrence file not found: {}".format(self.dwca_occurrence_file))

        self.has_multimedia_extension = os.path.exists(self.dwca_multimedia_file)

        if self.has_multimedia_extension:
            self.logger.info("DwCA folder has multimedia file")
        else:
            self.logger.info("DwCA folder has no multimedia file")

    def read_dwca_headers(self):
        self.read_dwca_occurrence_headers()
        self.read_dwca_multimedia_headers()

    def read_dwca_occurrence_headers(self):

        delimiter = utils._determine_csv_delimiter(self.dwca_occurrence_file,encoding="utf-8-sig")

        with open(self.dwca_occurrence_file) as csv_file:
            # dialect = csv.Sniffer().sniff(csv_file.read(1024))
            # csv_file.seek(0)
            # csv_reader = csv.reader(csv_file, dialect)
            csv_reader = csv.reader(csv_file,delimiter=delimiter)
            row1 = next(csv_reader)

        for key, val in self.occurrence_column_headers.items():
            if val in row1:
                self.occurrence_column_indices[key] = row1.index(val)

        if self.has_multimedia_extension and self.occurrence_column_indices["gbifID"] == None:
            raise ValueError("column not found in occurrence file: {}".format(self.occurrence_column_headers["gbifID"]))

        if not self.has_multimedia_extension and self.occurrence_column_indices["images"] == None:
            raise ValueError("column not found in occurrence file: {}".format(self.occurrence_column_headers["images"]))

        if self.occurrence_column_indices["taxon"] == None:
            raise ValueError("column not found in occurrence file: {}".format(self.occurrence_column_headers["taxon"]))

    def read_dwca_multimedia_headers(self):
        if not self.has_multimedia_extension:
            return

        delimiter = utils._determine_csv_delimiter(self.dwca_multimedia_file,encoding="utf-8-sig")

        with open(self.dwca_multimedia_file) as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=delimiter)
            row1 = next(csv_reader)

        for key, val in self.multimedia_column_headers.items():
            if val in row1:
                self.multimedia_column_indices[key] = row1.index(val)

        if self.multimedia_column_indices["gbifID"] == None:
            raise ValueError("column not found in multimedia file: {}".format(self.multimedia_column_indices["gbifID"]))

        if self.multimedia_column_indices["image"] == None:
            raise ValueError("column not found in multimedia file: {}".format(self.multimedia_column_indices["image"]))

        # print(self.multimedia_column_indices)

    def read_dwca(self):
        self.read_dwca_multimedia()
        self.read_dwca_occurrence()

    def read_dwca_multimedia(self):
        if not self.has_multimedia_extension:
            return

        delimiter = utils._determine_csv_delimiter(self.dwca_multimedia_file,encoding="utf-8-sig")

        with open(self.dwca_multimedia_file) as csv_file:
            c = csv.reader(csv_file,delimiter=delimiter)
            next(c)
            for row in c:
                self.multimedia_files.append({
                    "gbifID" : row[self.multimedia_column_indices["gbifID"]],
                    "image" : row[self.multimedia_column_indices["image"]]
                })

        # print(self.multimedia_files)

    def read_dwca_occurrence(self):
        delimiter = utils._determine_csv_delimiter(self.dwca_occurrence_file,encoding="utf-8-sig")

        with open(self.dwca_occurrence_file) as csv_file:
            c = csv.reader(csv_file,delimiter=delimiter)
            next(c)
            for row in c:
                if len("".join(row).strip())==0:
                    continue

                name = row[self.occurrence_column_indices["taxon"]]

                if self.has_multimedia_extension:
                    gbifID = row[self.occurrence_column_indices["gbifID"]]
                    for record in [x for x in self.multimedia_files if x["gbifID"] == gbifID]:
                        self.records.append({"name":name,"image":record["image"]})
                else:
                    for image in row[self.occurrence_column_indices["images"]].split("|"):
                        self.records.append({"name":name,"image":image})

        # print(self.records)

    def save_image_list(self):
        if not self.override_image_list is None:
            image_list = self.override_image_list
        else:
            image_list = self.image_list_file_csv

        with open(image_list, 'w') as csvfile:
            c = csv.writer(csvfile, delimiter=',', quotechar='"')
            for image in self.records:
                c.writerow([image["name"],image["image"]])
                self.total_images += 1
                self.classification_classes.append(image["name"])

        self.logger.info("wrote {} images in {} classes to {}".format(
            self.total_images,
            len(set(self.classification_classes)),
            image_list)
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--override_dwca_folder",type=str,help="specify alternative DwCA folder")
    parser.add_argument("--override_image_list",type=str,help="specify alternative image list filename")
    args = parser.parse_args()

    reader = DwcaReader()
    reader.set_debug(os.environ["DEBUG"]=="1" if "DEBUG" in os.environ else False)
    reader.set_project(os.environ)

    if args.override_dwca_folder:
        reader.set_override_dwca_folder(args.override_dwca_folder)

    if args.override_image_list:
        reader.set_override_image_list(args.override_image_list)

    reader.set_dwca_folder()
    reader.set_dwca_files()

    reader.read_dwca_headers()
    reader.read_dwca()

    reader.save_image_list()


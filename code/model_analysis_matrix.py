import os, sys, json, argparse, math
import tensorflow as tf
import numpy as np
import seaborn as sn
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from sklearn.metrics import classification_report
from lib import baseclass, dataset

class ModelAnalysisMatrix(baseclass.BaseClass):

    class_1 = None
    class_2 = None
    class_1_index = None
    class_2_index = None
    classes = None

    def __init__(self):
        super().__init__()

    def set_class_1(self,class_1):
        self.class_1 = class_1

    def set_class_2(self,class_2):
        self.class_2 = class_2

    def get_classes(self):
        f = open(self.class_index_model)
        self.classes = json.load(f)

    def get_analysis(self):
        f = open(self.get_analysis_path())
        self.analysis = json.load(f)

    def get_class_indices(self):
        if not self.class_1 == None:
            self.class_1_index = self.get_class_index(self.class_1)
            if self.class_1_index == None:
                self.logger.warning("class not found: {}".format(self.class_1))

        if not self.class_2 == None:
            self.class_2_index = self.get_class_index(self.class_2)
            if self.class_2_index == None:
                self.logger.warning("class not found: {}".format(self.class_2))

    def get_class_index(self,this_class):
        for i,c in enumerate(self.classes):
            if c == this_class:
                return i


    def print(self):
        if not self.class_1_index == None:
            print("{} ({})".format(self.class_1,sum([int(x) for x in self.analysis["confusion_matrix"][self.class_1_index]])))
            print("{}".format([int(x) for x in self.analysis["confusion_matrix"][self.class_1_index]]))
            print("")

        if not self.class_2_index == None:
            print("{} ({})".format(self.class_2,sum([int(x) for x in self.analysis["confusion_matrix"][self.class_2_index]])))
            print("{}".format([int(x) for x in self.analysis["confusion_matrix"][self.class_2_index]]))
            print("")

        if not self.class_1_index == None and not self.class_2_index == None:
            print("'{}' identified as '{}': {}".format(self.class_1,self.class_2,int(self.analysis["confusion_matrix"][self.class_1_index][self.class_2_index])))
            print("'{}' identified as '{}': {}".format(self.class_2,self.class_1,int(self.analysis["confusion_matrix"][self.class_2_index][self.class_1_index])))
            print("")

        if self.class_1_index == None and self.class_2_index == None:
            print("* all classes *")
            for i,c in enumerate(self.classes):
                print("{}".format(c))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--load_model",type=str,required=True)
    parser.add_argument("--class_1",type=str)
    parser.add_argument("--class_2",type=str)
    args = parser.parse_args()

    analysis = ModelAnalysisMatrix()

    analysis.set_project(os.environ)
    analysis.set_presets(os_environ=os.environ)
    analysis.set_model_name(args.load_model)
    analysis.set_model_folder()

    if args.class_1:
        analysis.set_class_1(args.class_1)

    if args.class_2:
        analysis.set_class_2(args.class_2)

    analysis.get_classes()
    analysis.get_analysis()
    analysis.get_class_indices()
    analysis.print()
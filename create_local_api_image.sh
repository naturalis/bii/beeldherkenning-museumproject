#!/bin/bash

# until this is fixed within official tensorflow-images:
# https://github.com/NVIDIA/nvidia-docker/issues/1632
# we use a locally built image, because that seems to circumvent gitlab CI/CD pipeline's caches

sudo docker build -t beeldherkenning-api:manual-build -f Dockerfile-api .
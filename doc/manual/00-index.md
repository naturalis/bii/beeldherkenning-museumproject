# Deep Learning pipeline for image recognition
## Index

+ [1. Installation](01-installation.md)
+ [2. Project definition](02-new-project.md)
+ [3. Acquiring data](03-adding-data.md)

Unfinished:
4. Preparing data
5. Classes
6. Training models
7. Analysing and comparing models
8. Inference

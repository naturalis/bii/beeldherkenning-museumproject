# Deep Learning pipeline for image recognition
## Installation & configuration

[Back to index](00-index.md)

### Server
Have access to a server with:
+ GPU
+ docker
+ docker-compose


### Repository checkout
Create a suitable base path (something like '/opt/deep-learning/'), and clone:

https://gitlab.com/maarten.schermer/deep-learning-tests.

This creates several folders. Most code is in 'code', but everything is run from the 'docker'-folder, which houses the docker-compose and .env files.


### Containers
Containers are built automatically by a CI/CD pipeline, but only when a new tag is pushed, or the pipeline is started manually. Because the actual code is not in any of the containers, there is no need to rebuild them on every commit.
Currently there are four containers:
+ registry.gitlab.com/maarten.schermer/deep-learning-tests/tensorflow
+ registry.gitlab.com/maarten.schermer/deep-learning-tests/tensorboard
+ registry.gitlab.com/maarten.schermer/deep-learning-tests/api
+ registry.gitlab.com/maarten.schermer/deep-learning-tests/webserver

The first one is the one through which all training is done. It currently is based on TensorFlow v2.7.0.


### Code vs containers
In order to avoid having to rebuild containers with every change to the code, the containers have been designed to only contain the runtime environment (i.e. Python, Keras and TensorFlow for the 'tensorflow'-container) but no code. The code is checked out onto the host, and made accessible to the container by mapping volumes, which is done in docker-compose.yml. Thus, running a script usually takes the form of executing a docker-run with the relevant script as parameter, like this:
```bash
docker-compose run <container name> <script name> <optional parameters>
```
Practical example:
```bash
docker-compose run tensorflow /code/model_trainer.py --override_image_list new_images.csv --override_class_list new_classes.csv

```
(depending on your severs configuration, this might require a preceding `sudo`)
As all configuration for the containers is in docker-compose.yml, docker run commands are executed from the directory containing that file (i.e. the 'docker' subdirectory).

Note that not all scripts actually require TensorFlow and Keras. For example, scripts dealing with the downloading of images don't actually use any deep learning-libraries. Nevertheless, for the sake of convenience, they are also executed using the same 'tensorflow'-container, even if, due to their size, they are a little slower to start than a theoretical container that only contains Python. One could create an extra container without TensorFlow and Keras for those jobs, but I never bothered. Just be aware that the fact that a script is run using the 'tensorflow'-container doesn't mean that there's actually any deep learning going on.



### Assigning a GPU
Docker requires a specific GPU to be assigned to the container that runs Tensorflow. You can see what GPU's there are by running the NVIDIA System Management Interface:

```bash
$ nvidia-smi

```
This will bring up an overview of the available GPU’s on your server, plus what's running:

```bash

Tue Apr 12 12:12:06 2022
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 460.91.03    Driver Version: 460.91.03    CUDA Version: 11.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  GeForce GTX 108...  Off  | 00000000:04:00.0 Off |                  N/A |
| 20%   41C    P2   180W / 250W |  10891MiB / 11178MiB |     11%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
|   1  GeForce GTX 108...  Off  | 00000000:06:00.0 Off |                  N/A |
| 16%   33C    P0    60W / 250W |      0MiB / 11178MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|    0   N/A  N/A     17861      C   python                          10889MiB |
+-----------------------------------------------------------------------------+

```
In this example, GPU #0 is currently in use, as can be seen from the memory usage for that processor, and from the process overview at the bottom.

Choose a free GPU - preferrably one that's going to be available for some time, so make sure other users of the same machine know which one you are using - and configure the GPU's identifying number as the value of 'NVIDIA_VISIBLE_DEVICES' in the 'tensorflow'-section of the docker-compose.yml file (which is in the ‘docker’ subdirectory). For example, using GPU number #1:
```yml
  [...]
  tensorflow:
    [...]
    environment:
      - NVIDIA_VISIBLE_DEVICES=1
  [...]

```
Not sure if this is also necessary if the server has only a single GPU, but it probably is.




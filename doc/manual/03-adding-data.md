# Deep Learning pipeline for image recognition
## Acquiring data

[Back to index](00-index.md)

What is needed to train a model, is a bunch of images and a list that specifies which image is which class. There are several ways these  can be provided. Choose the procedure below that matches your situation.


### From a DwC-A
If you have a Darwin Core Archive, you can unzip it to the 'dwca'-directory under your project root. Obviously, your archive needs to have multimedia URL's, which can be either in 'occurrence.txt', and/or, in case that your archive uses the multimedia extension, in 'multimedia.txt'. In these files, the script looks for columns with the headers "scientificName" and "associatedMedia" (occurrence.txt) and "identifier" (multimedia.txt).

You can now run:
```bash
sudo docker-compose run tensorflow /code/dwca_reader.py
```
which results in a file with classnames (taxon names) and image URLs, called 'images.csv', located in the 'lists' subdirectory.
The script takes to optional parameters:

```bash
--override_dwca_folder <alternative folder with your DwC files>
--override_image_list <full path of alternative image list file to write to>

```
Note that these folders should be specified as they are mapped in the container.

Next, download the actual images using:
```bash
sudo docker-compose run tensorflow /code/images_downloader.py

```
This will download all images from 'images.csv' into the 'images' subdirectory. Note that the script automatically creates subdirectories within 'images' to avoid having so many images in one folder that they become hard to manage. The script skips the downloading of images that were succesfully downloaded before, so you can safely restart the script if for some reason it halts halfway through.

It also creates the file 'downloaded_images.csv' in the 'lists' subdirectory, which contains the class and local filename for each succesfully downloaded image. This file is always (re)created completely, also when the script is restarted after halting halfway through.

'images_downloader.py' takes some optional parameters:
```bash
--override_image_list <alternative input image list>
--override_download_folder  <alternative download folder>
--override_output_file  <alternative output downloaded images list>
--path_prefix <prefix to prepend to the relative path in downloaded images list>
--image_list_delimiter  <CSV field separator. defaults to , (comma). use 'tab' for tab (\t).>
--request_timeout <timeout for image downloads in seconds. default 10s>
--include_original_data (switch to copy any extra columns from your input file to the output csv)
--no_session (switch to suppress the use of a TLS-session while downloading)
```



#### *A word on local filenames*
The downloader will infer a local filename from the URL by taking the basename. For instance:
```
https://upload.wikimedia.org/wikipedia/commons/c/c9/Meles_meles_01.JPG
```
will result in a downloaded file with the name:
```
Meles_meles_01.JPG
```

However, sometimes it is impossible to infer a local filename from the URL. An example is the URLs of the Naturalis medialibrary, which look like this:

https://medialib.naturalis.nl/file/id/L0819986_MLN/format/large

To save this as a local JPG-file, the script must somehow transform this URL into a filename. This is done using the optional IMAGE_URL_TO_NAME parameter, which is configurable in the .env file. It is specified as JSON, which in its most complete form looks like this:

```bash
IMAGE_URL_TO_NAME={"expression": "/file/id/|/format/[a-z]*", "replace": "", "postfix": ".jpg"}
```
During downloading, a local filename will be constructed applying a regular expression to the path of the URL (the part without the protocol specification and the domain), replacing everything that matches 'expression' with the string in 'replace'. After that, whatever is in 'postfix' will be appended to the string. Using the example above, this would follow these steps:

1. Have URL: `https://medialib.naturalis.nl/file/id/L0819986_MLN/format/large`
2. Use only the path: `/file/id/L0819986_MLN/format/large`
3. Apply the regular expression `"/file/id/|/format/[a-z]*"`, whicg matches `/file/id/` as well as `/format/large`,
   which are replaced with an empty string, which leaves `L0819986_MLN`
4. The postfix `.jpg` is added, resulting in the local filename `L0819986_MLN.jpg`

Both `expression`/`replace` and `postfix` are optional, so this would work as well:
```bash
IMAGE_URL_TO_NAME={"postfix": ".jpg"}
```
resulting in local filenames consisting of the basename of each URL appended with '.jpg'.

------

### From a folder structure
If you have a folder structure that uses classnames as sudirectories, you can use

```bash
sudo docker-compose run tensorflow /code/imagelist_from_class_folders.py \
  --image_root_folder <image root folder> \
  --imagelist_file <output file (full path)>
```
Both parameters are required (don't know why the second one doesn't just defaults to 'lists/downloaded_images.csv' - room for improvement!).
Please note that the images aren't moved from their location to the 'images' subdirectory, so if you want them there, move them manually before running this script.

There's a third, optional parameter:

```bash
  --imagelist_path_prefix <string to prepend to the filename in the output file>
```

------
### From an image list









--> results in:
  images/(all images)
  lists/classes.csv
  lists/downloaded_images.csv

You are now ready to [train your model](#training).


### Folder structure with images (where subfolders are classes)
TODO: put this in a conatiner, for consistency

Put the images in your 'images'-subfolder, retaining their the structure
run

sudo docker-compose run tensorflow /code/lists_from_folders.py

--> results in:
  classes.csv
  downloaded_images.csv


You are now ready to [train your model](#training).

### List of image URLs



### List of images, including images


### ALWAYS
Are there PNG's (or jpeg or gif) there?
sudo docker-compose run tensorflow /code/image_convert.py [--alt_image_list /path/to/downloaded_images.csv]




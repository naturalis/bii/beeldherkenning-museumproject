# **How to use this deep learning pipeline**

Below are all scripts of the pipeline with a short explanation of what they do.
They are roughly in the logical order of execution.

*** [Please read the files in the 'manual' folder for the first few steps in detail.](manual/00-index.md) ***

**Index**
+ [Project prepare](#prepare)
+ [Creating a class/image list](#image-list)
  + [From a Darwin Core file](#image-list-dwca)
  + [Manually](#image-list-manual)
+ [Acquiring images](#images)
  + [Downloading from list](#images-downloading)
  + [Images in local class folders](#images-class-folders)
  + Additional tools: [Image list change path](#images-change-path); [Image conversion](#images-convert); [Image check](#images-check)
+ [Classes](#classes)
  + [Create class list](#classes-list)
  + [Unified taxonomic classes from GBIF](#classes-unified")
  + [Getting other ranks from GBIF](#classes-other-ranks)
  + Additional tools: [Class translator](#classes-translator); [Filtering classes](#classes-filter)
+ [Training](#training)
  + [Running TensorBoard](#tensorboard)
  + [Multi GPU (experimental)](#training-multi)
+ [After training](#post)
  + [Analysis](#post-analysis)
  + [Report](#post-report)
  + [Comparing models](#post-compare)
  + [Confusion matrix values for two classes](#post-confusion)
+ [Inference](#inference)
+ Appendix:
  + [Identification style](#appendix-style)
  + [.env file](#appendix-env)

<a name="prepare"></a>
# Project prepare
Creates necessary project folders. Requires a root folder to be present in the [.env](#env) file.
```bash
docker-compose run tensorflow /code/project_prepare.py
```

<a name="image-list"></a>
# Creating a Class/Image list
<a name="image-list-dwca"></a>
## From Darwin Core files
Create image-list from DwC:
```bash
docker-compose run tensorflow /code/imagelist_from_dwca.py
```
By default options, the script reads from `dwca/occurence.txt` and, when it is present, `dwca/multimedia.txt`, and creates `lists/images.csv`, a list with class and URL for each image.

Arguments (optiona;):
+ `--override_dwca_folder` /path/to/alternative/dwca/folder/
+ `--override_image_list` /path/to/alternative/image_list.csv

<a name="image-list-manual"></a>
## Manually
Or create your own CSV-file with classnames and URLs called `images.csv` and put it in the `lists`-folder of your project.

<a name="images"></a>
# Acquiring images
<a name="images-downloading"></a>
## Downloading from 'lists/images.csv'
```bash
docker-compose run tensorflow /code/images_downloader.py
````
By default, the script reads from `lists/images.csv`, downloads to `images/` and writes to `lists/downloaded_images.csv`

While downloading to the `images/`-folder, subfolders are created on the fly to keep the number of files per folder manageable.

When run repeatedly, downloading of files that already exist is skipped, but a complete `downloaded_images.csv` file is always created.

Arguments:
+ `--override_image_list`: alternative input file
+ `--override_download_folder`: alternative download folder
+ `--override_output_file`: alternative output file
+ `--request_timeout`: for the downloader
+ `--path_prefix`: if the image paths need a standard prefix (if you have various sources in your project and want to use different image-folders for each, that still have to be read from a single downloaded_images file for training)
+ `--image_list_delimiter`: if it's not a comma (use 'tab' for tab)

**Make sure to read [Acquiring data](manual/03-adding-data.md) for details on how local filenames are constructed!!!**

<a name="images-class-folders"></a>
## Create downloaded image-list from class folders
If you have images that are in subfolders which represent the class:
```bash
docker-compose run tensorflow /code/imagelist_from_class_folders.py \
  --image_root_folder /data/new_images/ \
  --imagelist_file /data/projects/my_project/lists/downloaded_images.csv
````
Take note that these folders should be as they are 'seen' from *within the container* (check the volume mappings in `docker-compose.yml`)!

Some additional tools:

<a name="images-change-path"></a>
## Image list change path (optional)
For whhen the paths in your image file miss a prefix of the entire path must be replaced.
```bash
docker-compose run tensorflow /tools/image_list_change_path.py \
  --image_list_file /data/projects/my_project/lists/downloaded_images.csv \
  --output_file  /data/projects/my_project/lists/downloaded_images_fixed_path.csv \
  --path_prefix prefix
  --path_replace /new/folder/
  --csv_delimiter
```

<a name="images-convert"></a>
## Image convert (optional)
Converts PNG's to JPG's  (if any) in your project's 'image'-folder, and *rewrites* downloadad_images list.
```bash
docker-compose run tensorflow /code/images_convert.py \
  --override_image_list /path/to/downloaded_images.csv
```
`--override_image_list` is optional.


<a name="images-check"></a>
## Image check (optional)
Does some tests to track down corrupt images.
```bash
docker-compose run tensorflow /tools/images_check.py \
  --image_root /data/projects/my_project/images/ \
  --check_bands_only
```
`--check_bands_only` is optional (see script for details)

If bands must be changed (to RGB):
```bash
docker-compose run tensorflow /tools/images_convert_channels.py \
  --image_root /data/projects/my_project/images/
```

<a name="classes"></a>
# Classes
The training script needs a separare file with the classes. This can easily be created on the fly at the start of training, but I never got around to it. Room for improvement!

<a name="classes-list"></a>
## Create class list (required)
```bash
docker-compose run tensorflow /code/class_list_create.py
```
Arguments (optional):
+ `--override_image_list` /path/to/list.csv (alternative for lists/downloaded_images.csv)
+ `--override_class_list` /path/to/classes.csv (alternative for lists/classes.csv)

Below are some things you can do with your class names. These all work on the values in the `downloaded_images.csv` file. **If you run any of them, make sure always to run `code/class_list_create.py` again afterwards!**

<a name="classes-unified"></a>
## Unified taxonomic classes from GBIF (optional)
This script uses the GBIF API to resolve class names to something unified.
```bash
docker-compose run tensorflow /tools/class_name_resolver.py \
  --input_file /data/projects/my_project/lists/downloaded_images.csv \
  --output_file /data/projects/my_project/lists/downloaded_images_resolved.csv \
  --minimum_confidence_score 99 \
  --full_report_file /data/projects/my_project/resolve_report.csv \
  --keep_resolved_only
```
Last three arguments are optional:
+ `--minimum_confidence_score`: default = 75, which works fine.
+ `--full_report_file`: detailed report on the resolvement (default is no report)
+ `--keep_resolved_only`: only retains the records that can actually be resolved (off by default)

Obviously, this is only useful when dealing with taxa (animals, plants, fungi etc.).

<a name="classes-other-ranks"></a>
## Getting other ranks from GBIF (optional)
Same data, other classes
```bash
docker-compose run tensorflow /tools/gbif_higher_taxon_lookup.py \
  --input_file /data/projects/my_project/lists/downloaded_images.csv \
  --output_file /data/projects/my_project/lists/downloaded_images_genera.csv \
  --rank_to_retrieve genus \
  --force_canonical
```
See script for details.

Some extra tools for manipulating class names:

<a name="classes-translator"></a>
## Class translator using a file (class a &rarr; class b) (optional)
```bash
docker-compose run tensorflow /tools/class_name_translator.py \
  --input_file /data/projects/my_project/lists/downloaded_images.csv \
  --translator_file /data/projects/my_project/lists/fabrice/translator.csv \
  --output_file /data/projects/my_project/lists/downloaded_images_translated.csv
```
`--translator_file` is a CSV with two columns, first one with the values to lookup, second column with the value it then should be replaced with. If a class isn't found in the translator file, it is retained *as is*.

<a name="classes-filter"></a>
## Filtering image list with another list of classes to be exclusively included
```bash
docker-compose run tensorflow /tools/image_list_filter.py \
  --image_list_file /data/projects/my_project/lists/downloaded_images_resolved.csv \
  --class_filter_file /data/projects/my_project/lists/naturalis/newly_species_list.csv \
  --output_file /data/projects/my_project/lists/downloaded_images_new_species_only.csv \
  --output_file_skipped /data/projects/my_project/downloaded_images_new_species_only_skipped.csv
```
Uses a lookup file (simple list of classes) to filter out records whose class is not in the lookup file.


<a name="training"></a>
# Training
Where the magic happens. Most simply:
```bash
docker-compose run tensorflow /code/model_trainer.py
```
Reads the images listed in `downloaded_images.csv` from the `images/`-folder, and reads classes from `classes.csv`, and goes to work. (Hyper)parameters are defined in the [.env](#env) file.

Ofcourse, there's some options:
```bash
docker-compose run tensorflow /code/model_trainer.py \
  --override_image_list /data/projects/my_project/lists/downloaded_images_new_species_only.csv \
  --override_class_list /data/projects/my_project/lists/classes_new_species_only.csv \
  --dataset_note <note> \
  --load_model <model ID> \
  --skip_training \
  --save_resampled_list /data/projects/my_project/lists/resampled_list.csv
```
Arguments (all optional):
+ `--override_image_list`: alternative image list
+ `--override_class_list`: alternative class  list
+ `--dataset_note`: when you run `model_trainer`, you will be asked interactively to add a note. These notes can help you to keep apart the different versions that you train. If you use this option, the interactive prompt is skipped.
+ `--load_model`: model ID of an existing model (must be in the project's `models/`-folder) to load for further training (transfer learning). Without this parameter, training starts from a randomly initialized ImageNet-model. Haven't used this option for a while, not sure if it still works.
+ `--skip_training`: just test settings and quit.
+ `--save_resampled_list`: if you configure upsampling, you can save a list of the resulting data set.

Make sure to run this in a multiplexer and detach after training starts, so you won't lose your terminal if there's a hiccup in the network or you have to shut down your machine (training jobs can last for days).

You can let training run through the full number of epochs (specified in the [.env](#env)-file), or break it off (Ctrl+C) if you see the validation accuracy is no longer improving. The training software saves the latest version of the model after the first epoch has finished, and after that, each time after an epoch in which the vaidation accuracy inceased has finished, so you don't lose your model of you break off. Note that there is the possibility of using an 'early stopping'-callback, but at some point I stopped using it after witnessing a model improving again after a large number of epochs.

Please note that when you break off, it can take some time for the software to react. Also, it tends to hang after breaking, in that you're not returned control of the prompt even if the training has stopped. If that is the case, use `nvidia-smi` (in another terminal window) to determine the PID of the process (the PID's are at the bottom, referring back to the number of the GPU it is currently using), and then forcibly kill the process.

If the training crashes with Pillow errors, there's likely something wrong with one or more images. This can be several things:
+ It's a PNG (see PNG-to-JPG-convert tool above)
+ It's not RGB (see band-conversion-tool above)
+ It's not actually an image; this happens sometimes when you download images from a list of URLs from all over the place. The downloader is only so smart (i.e., not very) and doesn't verify its downloads. You might end up with txt-files saying "404" and thing like that.
+ It's a corrupt image (see check above)

In general, it's advisable to gather all your images and do a visual check of what it is you're actually working with before proceeding to training.


<a name="tensorboard"></a>
## Running TensorBoard
It's possbile to monitor the training remotely through TensorBoard. This requires the following:
+ Have a built TensorBoard container ([Dockerfile-tensorboard](https://gitlab.com/naturalis/bii/beeldherkenning-museumproject/-/blob/master/Dockerfile-tensorboard))
+ Navigate to the docker-folder and run [run-tensorboard.sh](https://gitlab.com/naturalis/bii/beeldherkenning-museumproject/-/blob/master/docker/run-tensorboard.sh). Start with `--clear-logs` to clear logs from previous training sessions. If you don't, you'll possibly see overlapping graphs in TensorBoard (i.e., a mess). If you *do* do this, do it **before** you start the actual training; if you clear the logs during training, the current logs will also be deleted, causing the training process to crash. Take note that there's a hardcoded path in [run-tensorboard.sh line 42](https://gitlab.com/naturalis/bii/beeldherkenning-museumproject/-/blob/master/docker/run-tensorboard.sh#L42) that is probably incorrect for your situation. Correct before running.
+ Set up an ssh-tunnel from your workstation, mapping port 6006 (unnecessary if you run the training on your workstation). [example bash file](https://gitlab.com/naturalis/bii/beeldherkenning-museumproject/-/blob/master/tensorboard_tunnel.sh)
+ Open a browser, and navigate to http://localhost:6006/


<a name="training-multi"></a>
## Multi GPU (experimental)
Optional multi-GPU, not really tested, hung at least once after 15 epochs (and is it actually any faster?).
```bash
docker-compose run -e NVIDIA_VISIBLE_DEVICES=0,1 tensorflow /code/model_trainer.py \
  --use_devices 2
```
Obviously, these devices (GPU's) must be available, both physically and in your container (hence `NVIDIA_VISIBLE_DEVICES=0,1`).
Ignore all the auto-shard messages.


<a name="post"></a>
# After training
<a name="post-analysis"></a>
## Analysis
After training, run an analysis over the entire training set to get model performance characteristics:
```bash
docker-compose run tensorflow /code/model_analysis.py --load_model <model id>
```
The results of these are written to the specific subfolder of your project's `models/`-folder. If, for instance, your model's ID is `20220504-073100`, all the files are in `/project_root/models/20220504-073100/`.
For an analyzed model, this folder will include:
+ `analysis.json`: analysis as JSON-file.
+ `architecture.json`: model architecture.
+ `classes.csv`: class list (used during training).
+ `classes.json`: class list (used by the API).
+ `confusion_matrix.png`: confusion matrix as an image (it's also in `analysis.json` as an actual matrix).
+ `dataset.json`: details on training parameters etc.
+ `downloaded_images.csv`: image list used during training run.
+ `history_plot_phase_0.png`: loss & accuracy plots of the training
+ `model.hdf5`: the file with the actual model.

The ID of your model is generated automatically and is logged to screen when training starts. Run `model_compare` (see below) to see all your models.

<a name="post-report"></a>
## Report
To display details of an earlier analysis:
```bash
docker-compose run tensorflow /code/model_report.py --load_model <model id>
```
<a name="post-compare"></a>
## Comparing models
To compare all the models you have trained:
```bash
docker-compose run tensorflow /code/model_compare.py [--cleanup --delete <model id,model id,...>]
```

<a name="post-confusion"></a>
## Confusion matrix values for two classes
See a confusion matrix detail for two specific classes of a model:
```bash
docker-compose run tensorflow /code/model_analysis_matrix.py \
  --load_model <model id> \
  --class_1 "Branta canadensis (Linnaeus, 1758)" \
  --class_2 "Tadorna ferruginea (Pallas, 1764)"
```

<a name="inference"></a>
# Inference
Identifying images is done by running:
```bash
docker-compose run tensorflow /code/images_identify.py
```
with the following possible arguments:

+ `--image <path>`: path of a local image to be identified.
+ `--image_folder <path>`: path of a local folder with images to be identified.
+ `--image_list <list.txt>`: list of local paths of images to be identified (one path per line).
+ `--image_csv_list <list.csv>:` idem, as CSV (see also next two options).
+ `--csv_delimiter <delimiter for image CSV list>`: CSV-delimiter in the file above (Python's sniffer doesn't seem to work properly).
+ `--csv_column <number of the image column for image CSV list>`: number of the relevant column in the CSV-file.
+ `--max_identifications <int>`: limit to number of images to identify (for testing).
+ `--model <model ID>`: ID of the model to use. If argument is absent, script tries for API_MODEL_NAME in [.env](#env). The actual model must be present in project's model folder.
+ `--identification_style <original|batch|both|batch_incl_original>`: style of the identification (see appendix below). Default is 'original'.
+ `--top <int>`: how many identifictions to return per image (ordered descendingly by probability); defaults to 3.
+ `--output_file <filepath, json or csv>`: file to write output to; script detects format from the extension and formats the results accordingly.
+ `--include_original_data`: add all data from the original input file to the output. Works only with `--image_csv_list` as input and a CSV file as `--output_file`. This can save you the trouble of having to merge the identification results back into a spreadsheet with additional data.
+ `--prepend_image_root_folder <path>`: prepended to the path of image(s) you are trying to identify (if your images are somewhere else than is specified in your CSV).
+ `--override_image_root_folder <path>`: will be prepended to *basename* of path of image(s) you are trying to identify.


Example use:
```bash
docker-compose run tensorflow /code/images_identify.py  \
  --model "20220308-092837" \
  --image_csv_list /data/projects/my_project/lists/commons_downloaded_images.csv \
  --csv_column 1 \
  --prepend_image_root_folder /data/projects/my_project/images/commons/ \
  --identification_style both \
  --top 5 \
  --output_file /data/projects/my_project/results/commons_20220308-092837.csv \
  --include_original_data
```
This will load model `20220308-092837` (which should be present at `/data/projects/my_project/models/20220308-092837`), and read all the data from the file `commons_downloaded_images.csv`. It will then load each subsequent image by reading its path from column 1, each time prepending `/data/projects/my_project/images/commons/` to that path for the correct file location, and have the model identify it. From the results, the five topmost identifications are extracted (or less, if the model has less than five classes) and written to `/data/projects/my_project/results/commons_20220308-092837.csv`. Due to the `--include_original_data` switch, all other data that was read from the input file is appended. This way, the output file becomes a copy of the input file, with the identifications (class and probability) prepended to each line. This can be ideal for communicating results back to whoever provided the images for identification.


# Appendix
<a name="appendix-style"></a>
## Identification style
Used during inference. Possible values:
+ `original`: Predict on original image only (default)
+ `batch`: Prediction on a batch of generated images, and returning the mean of the predictions of each of those images. Images are generated applying the transformations specified in `API_BATCH_TRANSFORMATIONS` on the original image. Batch size is determined by `API_BATCH_IDENTIFICATION_SIZE`.
+ `both`: Both of the above. Besides the standard predictions, output has an additional field `predictions_original` for the original image only.
+ `batch_incl_original`: As `batch`, but with the first augmented image of the batch replaced with the original image.


<a name="appendix-env"></a>
## .env file
The .env file contains most configuration settings. Below is a complete overview of possible variables.
```bash
# basic project info (both mandatory)
PROJECT_NAME="My cool project"
PROJECT_ROOT=/data/projects/my_project/

# setting to override the default (/data/projects/my_project/images/) (optional)
IMAGES_ROOT=/data/projects/my_project/better_images/

# rule for locally naming downloaded files; see also below this block (optional)
IMAGE_URL_TO_NAME={"expression": "/file/id/|/format/[a-z]*", "replace": "", "postfix": ".jpg"}

# optional different column numbers for the downloaded_images file (default is class 0, image 1).
# take care, these are used in some other scripts as well and can cause confusion. should be refactored.
IMAGE_LIST_CLASS_COLUMN=0
IMAGE_LIST_FILE_COLUMN=2

# employed architecture (other than InceptionV3 have hardly been tested)
BASE_MODEL=InceptionV3
#BASE_MODEL=MobileNetV2
#BASE_MODEL=ResNet50
#BASE_MODEL=VGG16
#BASE_MODEL=InceptionResNetV2
#BASE_MODEL=Xception

# minimum and maximum of images per class. minimum is mandatory, maximum optional (defaults to unlimited)
CLASS_IMAGE_MINIMUM=2
CLASS_IMAGE_MAXIMUM=50

# apply class weights (optional; default false)
USE_CLASS_WEIGHTS=True

# up- or downsampling ratio's; see below (optional; don't use both at the same time ;)
UPSAMPLING_RATIO=0.1
DOWNSAMPLING_RATIO=0.001

## hyperparamters
# bacth size (mandatory)
BATCH_SIZE=64

# test/validation split (optional; defaults to 0.2)
VALIDATION_SPLIT=0.3

# monitor checkpoint (mandatory)
CHECKPOINT_MONITOR=val_acc

# image augmentation settings as a JSON-string (optional; leave empty for no augmentation)
IMAGE_AUGMENTATION={ "rotation_range": 40, "shear_range": 0.2, "zoom_range": 0.2, "horizontal_flip": true, "width_shift_range": 0.2, "height_shift_range": 0.2, "vertical_flip": false }


# all values below are lists, as the training program can be made to go through serveral
# phases, each with its own settings. see below this block for more info.
# number of epochs (length of training)
EPOCHS=[ 50 ]

# initial learning rate plus the learning rate-reduction callback
LEARNING_RATE=[ 0.0001 ]
REDUCE_LR_PARAMS=[{"monitor": "val_loss", "factor": 0.1, "patience": 4, "min_lr": 1e-09, "verbose": 1}]

# whether to freeze any layers out of training
FREEZE_LAYERS=[ "none" ]

# parameters for the early stopping callback (which i've stoped using)
EARLY_STOPPING_MONITOR=[ "val_loss" ]
EARLY_STOPPING_PATIENCE=[ 10 ]


# parameters for inference (see Appendix, sectoion on 'Identification style')
API_BATCH_IDENTIFICATION_SIZE=8
API_BATCH_TRANSFORMATIONS={"width_shift_range": [-0.2, -0.2], "height_shift_range": [-0.2, -0.2], "rotation_range": 0.1, "zoom_range": 0 }

```

### IMAGE_URL_TO_NAME
For details on how `IMAGE_URL_TO_NAME` works, see [this page](manual/03-adding-data.md).

### Defining training phases
Some parameters are specified as lists, as the training program can be made to go through serveral phases, each phase with its own settings. The driver for this is the value of `EPOCHS`; if `EPOCHS` has only one value, there will be one phase. If it has two, there will be two phases et cetera.
The length of each phase is determined by the corresponding value of `EPOCHS`. Setting `EPOCHS=[ 5, 50 ]` will result in two phases, one of 5 epochs, and following that another one of 50 epochs. For the other parameters that take multiple values (`LEARNING_RATE`, `REDUCE_LR_PARAMS`, `FREEZE_LAYERS`, `EARLY_STOPPING_MONITOR`, `EARLY_STOPPING_PATIENCE`), for each phase, the corresponding value from the list will be selected. So, `FREEZE_LAYERS=[ "base_model", "none" ]` will make the layers of the base model be frozen in the first phase, and no layers to be frozen in the second. If a parameter has more items in its list then there are phases, the superfluous ones are ignored. If they have less, their last value will be carried over to the next phase.

### Resampling
When you have imbalanced data, there's an option to upsample - add duplicates of the images of underrepresented classes - or downsample - take out random images of overrepresented classes. This is triggered by setting either the `UPSAMPLING_RATIO` or the `DOWNSAMPLING_RATIO` (if you set both, downsampling is ignored). The ratio's are the target ratio's of the number of images in the most underreprsented and in the most overrepresented classes. If the original ratio is already larger than the one you specify, no resampling happens.
